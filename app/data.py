Accounts = {}
Groups = {}
Events = {}

from app.sign import Person, Group, search

'''
    {'t':'p', 'name':'심건영', 'email':'kyshim9004@gmail.com', 'mobile':'010-9919-9004', 'private':'xprv9ugRLzC5L34qb8K31HTYxFomfrqZudvN72vWcztpT393tgkewUuPShAvHtJDjb7rEzRHiKnm6mcLu9nKUHyMUPjCCMkfjBfVTJCoWWScu62', 'seed':'repeat void negative suggest wrap copper verb bird vault desk chief aspect', 'address':'blinij9', 'eth':5, 'id':'VET3Z46O'},
    {'t':'p', 'name':'길라임', 'email':'gillaim@gillaim.com', 'mobile':'010-1234-4321', 'private':'xprv9uXhCFr4V6HYJsP5SA2iPUaBZw1VbMmik29ycAmXhGKPkbSTvRQ62oZYM7jQxE63TqcTjUZxtWzU6vFSy6hzJmLh66s49LxSe9Q9ZRBzeim', 'seed':'mix impact dignity taxi buzz jump gauge require busy ritual fever steel', 'address':'1nglrp8', 'eth':503, 'id':'LS827AKJ'},
    {'t':'p', 'name':'Dustin Nippert', 'email':'nippert@dustin.com', 'mobile':'010-1111-2222', 'private':'xprv9u6ecE2bVkvR1tFNoAAuxkmTJ2RC9NxGppGS2EeNWgyMmCyhbXuwfyZ6Eqs1t1r89xXhPAQEgRTGbWZq69XcvntDTrvSUYkPZm6gxdcLaN1', 'seed':'make thing keep ritual spirit enough off iron supreme divert burger head', 'address':'37kp53b', 'eth':203, 'id':'FTI0R7M2'},
    {'t':'p',  'name':'한일석', 'email':'ilsuk@gmail.com', 'mobile':'010-2435-2483', 'private':'xprv9ugcFEVwtuznigFcSDM87V6AuJ8dhXpkfE6ZQET2DPAUsb7Zrx9vTytTJKGRkfxVYZU8BKRBe5gz6eAmajXfzrc9Vke2cN4btFLem1HcTBD', 'seed':'stool small answer exile tone jump drift student future tongue street rocket', 'address':'a2v2dh5', 'eth':0, 'id':'48293433'},
    {'t':'p', 'name':'김동일', 'email':'dong1@gmail.com', 'mobile':'010-2533-9907', 'private':'xprv9u64ySjsbukpc2mdWEyEQDQorTW9DmxwUEsTMqiJhbnTTe8x8CvSABsPk3hWqaXye6m4xEtVMpXLkmKNZZPg1JmKPr3CeCG1gnrUkY5TCm9', 'seed':'salmon super coast beauty cable crawl entry lift own midnight veteran sport', 'address':'g14gdsf', 'eth':0, 'id':'4873459'},
    {'t':'p', 'name':'이원희', 'email':'1hi@gmail.com', 'mobile':'010-3880-5785', 'private':'xprv9ukU6X1mZ97pyrKyqGVuV58yvvCD4PjkPxbJhEP2A6cCpH4BrrX7xAKzDK8Bk9NDfLLx5QTNkeX2GWNY1N2Z3GY21ZbBPapeNJRXTnUnzZd', 'seed':'turn primary plastic release practice wait genuine allow sure expire immense bamboo', 'address':'u35o2jf', 'eth':0, 'id':'89332154'},
    {'t':'p', 'name':'박성수', 'email':'wolfboy@kyonggi.ac.kr', 'mobile':'010-5319-0309', 'private':'xprv9uaU7sLG7eTAFWAthwu4CW8n8svBGvp5d6THKay39zHMNC3hsvQzXuDNenwJMibUr1gzkozYKekP8agiAoCSFZW2NTkPom8891QfaThReQw', 'seed':'choose brief rice buzz clump column invest spray among pact bike impose', 'address':'ks993gg', 'eth':0, 'id':'89332154'},
    {'t':'p', 'name':'이승진', 'email':'lsg0385@naver.com', 'mobile':'010-2470-8745', 'private':'xprv9vjJCGtnZbV6kcPZW7qYvACoYHhHvYPW6iGiTtiKAP5oADioMQ2E8h9xQhK81HY1htEw1XCmEWQqTHqLaDzLG7ULf77p3WPqJdNKaBDWez7', 'seed':'midnight deputy hurry dove base stuff hunt usual swap ceiling neutral what', 'address':'o0ln4wo', 'eth':0, 'id':'89332154'},
    {'t':'p', 'name':'이재현', 'email':'ljh1997213@gmail.com', 'mobile':'010-2852-4280', 'private':'xprv9vBxgRJqPLBRRC5QxGjAR7WEe5gV23JQzMDvMABcgRCfVRqGnZ2J3Y4Lzng7GKjbAnGDWLduDmwyCyVekkUjZfSHsvZ1qoUmwqhLutJq7Sq', 'seed':'hole measure oxygen business confirm weird rice close portion reject rebel monkey', 'address':'nk6i630', 'eth':0, 'id':'89332154'},
    
    {'t':'g', 'name':'team ilsuk', 'email':'nonghwal.teamilsuk.com', 'mobile':'031-111-1111', 'private':'xprvA1RDb2gFcRP4m9i2HbsiukxCrB7beqCZzsLj8Ky3XSFhuo7QEbSJPjrCAiuf7SYC7bnnoSkELbba8JLs8TzNbBLzLhRKwUeihcyAJfnvMbB', 'seed':'brisk leave ship noble auto nasty gain expose crater friend deer cinnamon', 'address':'aULf8y', 'eth':'2', 'code':'EC0001', 'state':'이천시', 'type':'농촌', 'msg':'농사철 부족한 농촌 일손을 도와주기 위해 설립된 단체입니다.', 'cnt':'23023'},
    {'t':'g', 'name':'EnEn', 'email':'admin@EnEn.kr', 'mobile':'031-222-3333', 'private':'xprv9yrSu6qtwAXEVhjn473VEsdTMLgfPUGE6Nihe1Dg5rsKEwcECxFZGfRbyuJjWU1gCgveVSHthPX4aDQsFYvXM767vfT6L2Gs1sm796nYwiY', 'seed':'tower lake window donkey point mango soul approve cement practice enlist lemon', 'address':'BKBvhD', 'eth':'100', 'code':'GP0001', 'state':'가평군', 'type':'환경', 'msg':'여행지에 버려진 쓰레기들을 치우는 활동을 합니다. Save Earth, Save Us', 'cnt':'231'},
    {'t':'g', 'name':'1석2조', 'email':'ilsuk2@gmail.com', 'mobile':'010-2435-2485', 'private':'xprv9xo9kgP3vryRA9haib5yKyE9yUBDYCJjStRjkgpu37z4AK8A7MnxC8kuxLq3nNemrXn4sBj4qpTCe7H2XuMvuurhgFnuf6ucn637E8HaYGq', 'seed':'shallow evolve dirt sense wire spare exist time bright deer access arrive', 'address':'111111', 'eth':'1000', 'code':'SWJA0005', 'state':'수원시 장안구', 'type':'개발', 'msg':'블록체인 해커톤에 참여하여 최우수상을 타고 돌아올 분들을 구합니다.', 'cnt':'5'},
    {'t':'g', 'name':'삼사삼사', 'email':'3434@goodthing.com', 'mobile':'031-3434-3434', 'private':'xprv9xmRUcbHYJFyUseS5pZG1zY9SM44JEVW3EzHnQfgffTGu51mozf7ZPLYSpciycLuT9B4HQY9JzaqiSSGfhjjssuT7HqVQ1m7MxNS1u5Copn', 'seed':'excuse domain ramp fault unhappy puzzle fence myself oak present produce soccer', 'address':'NlAHSM', 'eth':'34', 'code':'SWGS0100', 'state':'수원시 권선구', 'type':'돌봄', 'msg':'3분이서 4개의 가구의 독거 노인을 보살피는 활동입니다.', 'cnt':'3434'},
    {'t':'g', 'name':'이재명 경기도 지사님의 법률상담소', 'email':'ljm@lawconsulting.com', 'mobile':'031-000-0000', 'private':'xprv9xmRUcbHYJFyUseS5pZG1zY9SM44JEVW3EzHnQfgffTGu51mozf7ZPLYSpciycLuT9B4HQY9JzaqiSSGfhjjssuT7HqVQ1m7MxNS1u5Copn', 'seed':'oyster message often excuse pattern penalty dad jaguar fabric ostrich east chapter', 'address':'Nbnm18', 'eth':'1000000', 'code':'SN0001', 'state':'성남시', 'type':'시설', 'msg':'이재명 경기도 지사님의 무료 법률 상담 봉사활동 개최시 필요한 시설물 보조 관리 인력 모집합니다.', 'cnt':'111111'},
    {'t':'g', 'name':'수정공원 사랑이', 'email':'crystal@park.com', 'mobile':'031-111-1234', 'private':'xprv9ximoAc1uyfEM3DfXStssoTF4bfWLPXfo6gdBskZjb4Xd628c5HZPraW4Vj2k11BSRCRNdBmArTywnudcac1LjfLGY6wuR4EPvEzPeazcK9', 'seed':'page pledge deer believe match pledge throw ocean snap replace identify easy', 'address':'Vxmftm', 'eth':'200', 'code':'HW0001', 'state':'화성시', 'type':'환경', 'msg':'화성시 공원청소 하실 인원 구합니다.', 'cnt':'13'},
    {'t':'g', 'name':'금 공원 지키미', 'email':'gold@park.com', 'mobile':'031-222-1234', 'private':'xprv9yCBsFBKqjftbfbZTnX45Y8WTCqg3R1C3KEvELDbdicShsMmZ8MaaosvWaqtPYLo1ovUs9REYDgeVU3Kyp7dzk29V3ipP8qQ3UheZ1WoNRm', 'seed':'cable defense acid begin exchange charge peanut warfare evolve virus corn nation', 'address':'asVgp2', 'eth':'22', 'code':'BC0001', 'state':'부천시', 'type':'환경', 'msg':'부천시 공원청소 하실 인원 구합니다.', 'cnt':'20'},
    
'''

users = [
    {'t':'p', 'name':'심건영', 'email':'kyshim9004@gmail.com', 'mobile':'010-9919-9004', 'private':'xprv9ugRLzC5L34qb8K31HTYxFomfrqZudvN72vWcztpT393tgkewUuPShAvHtJDjb7rEzRHiKnm6mcLu9nKUHyMUPjCCMkfjBfVTJCoWWScu62', 'seed':'repeat void negative suggest wrap copper verb bird vault desk chief aspect', 'address':'blinij9', 'eth':'5', 'id':'VET3Z46O', 'level':'1'},
    {'t':'p', 'name':'길라임', 'email':'gillaim@gillaim.com', 'mobile':'010-1234-4321', 'private':'xprv9uXhCFr4V6HYJsP5SA2iPUaBZw1VbMmik29ycAmXhGKPkbSTvRQ62oZYM7jQxE63TqcTjUZxtWzU6vFSy6hzJmLh66s49LxSe9Q9ZRBzeim', 'seed':'mix impact dignity taxi buzz jump gauge require busy ritual fever steel', 'address':'1nglrp8', 'eth':'503', 'id':'LS827AKJ', 'level':'1'},
    {'t':'p', 'name':'Dustin Nippert', 'email':'nippert@dustin.com', 'mobile':'010-1111-2222', 'private':'xprv9u6ecE2bVkvR1tFNoAAuxkmTJ2RC9NxGppGS2EeNWgyMmCyhbXuwfyZ6Eqs1t1r89xXhPAQEgRTGbWZq69XcvntDTrvSUYkPZm6gxdcLaN1', 'seed':'make thing keep ritual spirit enough off iron supreme divert burger head', 'address':'37kp53b', 'eth':'203', 'id':'FTI0R7M2', 'level':'1'},
    {'t':'p',  'name':'한일석', 'email':'ilsuk@gmail.com', 'mobile':'010-2435-2483', 'private':'xprv9ugcFEVwtuznigFcSDM87V6AuJ8dhXpkfE6ZQET2DPAUsb7Zrx9vTytTJKGRkfxVYZU8BKRBe5gz6eAmajXfzrc9Vke2cN4btFLem1HcTBD', 'seed':'stool small answer exile tone jump drift student future tongue street rocket', 'address':'a2v2dh5', 'eth':0, 'id':'48293433', 'level':'2'},
    {'t':'p', 'name':'김동일', 'email':'dong1@gmail.com', 'mobile':'010-2533-9907', 'private':'xprv9u64ySjsbukpc2mdWEyEQDQorTW9DmxwUEsTMqiJhbnTTe8x8CvSABsPk3hWqaXye6m4xEtVMpXLkmKNZZPg1JmKPr3CeCG1gnrUkY5TCm9', 'seed':'salmon super coast beauty cable crawl entry lift own midnight veteran sport', 'address':'g14gdsf', 'eth':0, 'id':'4873459', 'level':'2'},
    {'t':'p', 'name':'이원희', 'email':'1hi@gmail.com', 'mobile':'010-3880-5785', 'private':'xprv9ukU6X1mZ97pyrKyqGVuV58yvvCD4PjkPxbJhEP2A6cCpH4BrrX7xAKzDK8Bk9NDfLLx5QTNkeX2GWNY1N2Z3GY21ZbBPapeNJRXTnUnzZd', 'seed':'turn primary plastic release practice wait genuine allow sure expire immense bamboo', 'address':'u35o2jf', 'eth':0, 'id':'89332154', 'level':'3'},
    {'t':'p', 'name':'박성수', 'email':'wolfboy@kyonggi.ac.kr', 'mobile':'010-5319-0309', 'private':'xprv9uaU7sLG7eTAFWAthwu4CW8n8svBGvp5d6THKay39zHMNC3hsvQzXuDNenwJMibUr1gzkozYKekP8agiAoCSFZW2NTkPom8891QfaThReQw', 'seed':'choose brief rice buzz clump column invest spray among pact bike impose', 'address':'ks993gg', 'eth':0, 'id':'89332144', 'level':'5'},
    {'t':'p', 'name':'이승진', 'email':'lsg0385@naver.com', 'mobile':'010-2470-8745', 'private':'xprv9vjJCGtnZbV6kcPZW7qYvACoYHhHvYPW6iGiTtiKAP5oADioMQ2E8h9xQhK81HY1htEw1XCmEWQqTHqLaDzLG7ULf77p3WPqJdNKaBDWez7', 'seed':'midnight deputy hurry dove base stuff hunt usual swap ceiling neutral what', 'address':'o0ln4wo', 'eth':0, 'id':'89332134', 'level':'1'},
    {'t':'p', 'name':'이재현', 'email':'ljh1997213@gmail.com', 'mobile':'010-2852-4280', 'private':'xprv9vBxgRJqPLBRRC5QxGjAR7WEe5gV23JQzMDvMABcgRCfVRqGnZ2J3Y4Lzng7GKjbAnGDWLduDmwyCyVekkUjZfSHsvZ1qoUmwqhLutJq7Sq', 'seed':'hole measure oxygen business confirm weird rice close portion reject rebel monkey', 'address':'nk6i630', 'eth':0, 'id':'89332124', 'level':'2'},

    {'t':'g', 'name':'배움을나누는사람들', 'email':'contact@edushare.kr', 'mobile':'null', 'private':'xprv9vVS2GFGzA9roXgBFNCz5UXDGXitFRV93yyhbEVNqJhqzqSuNXgG73y9SCEKUU4UGnJWraMwQ68xcTgPPuAoaL4nBcpzuBFv4RMuwTAZBVo', 'seed':'wing vessel famous task type attack force drum coin drift wrong thing', 'address':'6B7b7y', 'eth':320, 'code':'EW0001', 'state':'의왕시', 'type':'교육', 'msg':'"배움을 나누는 사람들"은 국내 최대 규모 대학생 비영리 교육봉사단체로, 서울, 의왕, 부산 등 전국에 분포되어 있는 교육장을 중심으로 전국의 대학생이 모여 중학생들에게 양질의 교육을 제공하고 있습니다. 현재 2007년 이후로 12년 동안 만 명 이상의 교사와 학생들이 활동했으며 8개의 교육장에서 교육을 이어나가고 있습니다. 교육의 기회로부터 소외된 학생들이 다시 자신감을 가질 수 있도록, 그리고 이를 통해 교육의 선순환 구조를 만들어낼 수 있도록, 여러분의 많은 지원 부탁드립니다.', 'com':'배움을나누는사람들은 최고의 교육봉사 단체입니다.', 'cnt':'17235'},
    {'t':'g', 'name':'일석회', 'email':'hwanhee@bongsa.com', 'mobile':'010-0101-0101', 'private':'xprv9v4C7fWkBTyus7ZJKYjw4avbtaKKUNwQUrjqNanC7FQa8yJYUaqGssKXPYgByktV3NvB387HL7ZZjGU26QvSWGZhXGZrb3WBfHnPqZQh5mF', 'seed':'dragon expose never delay equip narrow idle type hospital once decide gold', 'address':'V4BTBJ', 'eth':100, 'code':'OS0001', 'state':'오산시', 'type':'미용', 'msg':'군인 및 취약계층에게 미용 서비스를 무료로 제공하는 봉사활동 단체', 'com':'머리도 잘 잘라요.', 'cnt':'23'},
    {'t':'g', 'name':'일석요양원', 'email':'sukrest@gmail.com', 'mobile':'010-9348-5624', 'private':'xprv9vdewLwsSyWjxZy8a7myWXehF72f2wEKjUYVhdqGiaoBdScKv1C6qoqrSKF3wHmhex8CwsghWUdqLf16YJ7tU88gw52Hdu8NZGKELGZaEZR', 'seed':'put trash ethics world curtain dolphin island patrol sister canvas illness vote', 'address':'u35o2jf', 'eth':0, 'code':'SWPD0121','state':'수원시 팔달구', 'type':'돌봄', 'msg':'저희는 경기도 최고의 시설을 자랑하는 일석요양원입니다.', 'com':'허리가 조금 아프군요','cnt':20},
    {'t':'g', 'name':'일석병원', 'email':'sukhospital@gmail.com', 'mobile':'010-3553-5678', 'private':'xprv9v9RTKUtb1AGB6HUSuorcvVckwYztJ8978QjPmFmjNdGose5JHAWvGTub1wbzAsfEeSpktqgp1kPkqxwtsc2ZsC3JKhBmA8fr3HY3yZhZj1', 'seed':'produce patch margin again venue awake own current will pole travel mushroom', 'address':'y3t4i4i', 'eth':0, 'code':'SWJA0001', 'state':'수원시 장안구', 'type':'의료', 'msg':'저희는 경기도 최고의 의료진을 자랑하는 일석병원입니다.', 'com':'1등하고싶어요', 'cnt':40},
    {'t':'g', 'name':'일석이조', 'email':'ilsuk2jo@gmail.com', 'mobile':'010-5244-5244', 'private':'xprv9uAvnpJiP5PbN66yUzicWM1s4u8WB3qwXzgbUPzTUJ5XF7RnsoYkeyHbwrDdQBKB2Ggfo2kThnBj4rHzvgEjrugZC5LySWo5YJNgHTECqSv', 'seed':'legal silent crime minute ski scan protect river surge this appear quit', 'address':'324gwrd', 'eth':0, 'code':'SWJA7777', 'state':'수원시 장안구', 'type':'개발', 'msg':'저희는 최고의 개발자가 되고싶은 일석이조입니다', 'com':'열심히 개발합시다', 'cnt':5},
    {'t':'g', 'name':'태양 노양원', 'email':'sun@old.com', 'mobile':'031-333-1234', 'private':'xprv9yyX4ixvRpZqkUwMpPJzykK7cxZ2KZLu5WDXLJP3MRcCa7eGNTkJ81SEj9w7RvnND31TRbdzdThqdwBNHpGV2dA4XvCkdFGzeSF5iwHMAgD', 'seed':'human smoke inherit behave tell clap buzz common ride devote vendor bean', 'address':'7tamwa', 'eth':'50', 'code':'GP0001', 'state':'군포시', 'type':'돌봄', 'msg':'우리나라 최고의 노인 복지 시설입니다.', 'cnt':'5103'},
    {'t':'g', 'name':'평창 올림픽 단기', 'email':'pyonchang@olympic.com', 'mobile':'033-444-1234', 'private':'xprv9xxar7pFyhTdUhpoEvLAJ8SNfLZVic7tFbAxzqepKCyPqmcgDaZGErWXHuUurU4HtnYbaiG9VTdnh2b1U9uJtTfQaA7nRJQULaRSk4HATsU', 'seed':'window engine pole april warfare blur rabbit best lecture cost erupt agree', 'address':'5AruXo', 'eth':'150', 'code':'PC0001', 'state':'평창군', 'type':'시설', 'msg':'올림픽 기간 시설물 관리를 도와주실 분 구합니다.', 'cnt':'10000'},
    {'t':'g', 'name':'VANK_GG', 'email':'vank_gg@vank.com', 'mobile':'031-666-1234', 'private':'xprv9y1AoqheYRhyga87ipfJg1Zt46T58usMobMMq1A2NqSGap1ngDWAjn6HuXkNHJHnv5717MHGzPaLY3ixzckbMBQCoWwjcTsa9AMz23o7hhr', 'seed':'patient believe razor muffin rabbit chronic summer blanket bamboo truly plate claw', 'address':'OL3nqe', 'eth':'170', 'code':'HW0002', 'state':'화성시', 'type':'홍보', 'msg':'우리나라 역사 알리미를 구합니다.', 'cnt':'250'},
    {'t':'g', 'name':'양평군 환경 보전 센터', 'email':'yplove@yp.com', 'mobile':'031-4321-1234', 'private':'xprv9ySJd7E79s4vocAs9eStgCmFnCbQxcS9KnzvpW5vcaqiJ3CcvUyx2jctx5GDbKPSmnCuHKhkKdYQYkdtYMGcHHf9FGNSMDCNeWBLGWDWJ4y', 'seed':'boost quiz setup oppose silly expose endorse neck kind hungry before aerobic', 'address':'KfhS0M', 'eth':'31', 'code':'YP0001', 'state':'양평군', 'type':'환경', 'msg':'물좋고 공기좋은 양평군, 자연환경을 같이 보전하실 분 구합니다.', 'cnt':'223'},
    {'t':'g', 'name':'독산성 보호소', 'email':'doksanseong@osan.com', 'mobile':'031-2323-1234', 'private':'xprv9yGfKJ7NA9qAssXnw1m6TP9YwkWYcq3EXKez9ZMR76YJH9F2vhr5ZHNtjtc3adLpefYyPthF6k1JymFmz3Z59LmZAzPz3gBwhvGHPtngioQ', 'seed':'brand rude pause system current near tired portion come wrestle suspect opinion', 'address':'nKALs7', 'eth':'200', 'code':'OS0002', 'state':'오산시', 'type':'시설', 'msg':'오산시 독산성을 지켜주실 시민 여러분을 모집합니다.', 'cnt':'89'},
    {'t':'g', 'name':'연천 다이노소어스', 'email':'dinosaurs@yeoncheon.com', 'mobile':'031-3232-1234', 'private':'xprv9xfZWB6van9UNYXidys66BLuAmyzrZytTrViQ71azEjgNv3v7JkG3yj7WTQcaPep3XnhFZmrqN77mjVjvTmLhknKCzXHzd9GDznDpJswKvA', 'seed':'inform gain fly brush floor task faculty idea inflict dream paddle agree', 'address':'Lfxum2', 'eth':'30', 'code':'YC0001', 'state':'연천군', 'type':'시설', 'msg':'공룡의 도시 연천군의 공룡 박물관 시설을 관리하는데 도움을 주실 분이 필요합니다.', 'cnt':'21'},
    {'t':'g', 'name':'이석삼조', 'email':'2323@leesam.com', 'mobile':'070-2323-2323', 'private':'xprv9yZd1aWZPpBhGd6X5pgoXZjWhRHNVqWuUmMRtzjLxwpQh2eDFCGPMSLGYHbsjujNTH6e681ck87j6axJj3mva6bGREcCr5KcUx5FC9ZyZ57', 'seed':'undo hobby galaxy large myth another husband media meat theory giggle phrase', 'address':'Vxmftm', 'eth':'23', 'code':'GP0001', 'state':'김포시', 'type':'개발', 'msg':'일석이조가 1등 할 거 같습니다. ㅜㅜ', 'cnt':'5'},
    {'t':'g', 'name':'일석공원', 'email':'ilsuk@park.com', 'mobile':'070-1111-1234', 'private':'xprv9yMEbc9r4jkuyvDoo2KZFyKWyb1StvejKqHg7TPx3ndj85A19S2t65R4rW7YGXZbTGddGpTtpLEPQ61cRyDLehWLSuiPSYaDAC8MTYXCzht', 'seed':'glow federal rail crime horse place poverty skate retreat milk morning depth', 'address':'lB77ge', 'eth':'100', 'code':'GY0001', 'state':'고양시', 'type':'환경', 'msg':'일석공원 공원청소 하실 인원 구합니다.', 'cnt':'20'},
    {'t':'g', 'name':'건영병원', 'email':'ky@hospital.com', 'mobile':'031-111-0123', 'private':'xprv9yY7e2GbBr814SDMGBGehTCRjpZXL15k4znggwVd68XPRJCNFawo7JrD9pz8W5XVuVW7aJBvTZbDykBNkizfhM4CYPHuZkNHc9BRGE9YEhE', 'seed':'spring gauge equal lounge gift disorder elevator cube boost find filter core', 'address':'YN5PN0', 'eth':'55', 'code':'OS0123', 'state':'오산시', 'type':'의료', 'msg':'건영병원 청결을 도와주세요~', 'cnt':'10000'},
    {'t':'g', 'name':'일석고아원', 'email':'ilsuk@orphan.com', 'mobile':'031-1111-9494', 'private':'xprv9zUysNPgZK6hT41BgYs1riEBuhsLRLXLCDyQSikbdCocKy6D8Cv6kdrJVfsaKoA7omAZmr7omL5F9deAJoS6az1JWqVK2kLDzCm7KDv97vc', 'seed':'flag reveal liar syrup practice spring buzz couple initial few shoe round', 'address':'njmXLk', 'eth':'12', 'code':'AY0001', 'state':'안양시', 'type':'돌봄', 'msg':'고아원에서 아이들을 같이 보살펴주실 분 구합니다. 오래 다니실 수 있는 분이면 좋습니다.', 'cnt':'39'},
    {'t':'g', 'name':'건영공부방', 'email':'kys@gongbu.com', 'mobile':'031-5959-5959', 'private':'xprv9xpAs6ovHUMb9A8jkKRNY3ATwSYZunDzD3JsuztbGSekPKv32CdgcFYiAXELpYV4n8ZWpJ4Cj7GAyzrwhgLzVLwws93T8fJYJwFRnMnBeFQ', 'seed':'planet suffer before dog raven casino armed soap control elegant absurd dance', 'address':'R0HGOs', 'eth':'23', 'code':'PC0001', 'state':'포천시', 'type':'교육', 'msg':'아이들 무료 공부방 교사 모집합니다.', 'cnt':'5'},
    {'t':'g', 'name':'일석이조 공부방', 'email':'ilsukejo@gongbu.com', 'mobile':'070-0022-1234', 'private':'xprv9ySWdgsMmY2Tk4bYUn9PkEYg4SWFqnNKJScdtyXDMAZz87MrVYsMFiWTq9jdRxds52YDYcPEN1gfwjKh2zngUKyMSpM6HJJ7HxWe4qBUtTo', 'seed':'nominee spice alley asthma sustain champion life oil miracle insane theme night', 'address':'n8er6A', 'eth':'2', 'code':'DDC0001', 'state':'동두천시', 'type':'교육', 'msg':'동두천의 자랑, 일석이조 공부방 교사 회원을 모집합니다.', 'cnt':'6'},

    {'t':'g', 'name':'team ilsuk', 'email':'nonghwal.teamilsuk.com', 'mobile':'031-111-1111', 'private':'xprvA1RDb2gFcRP4m9i2HbsiukxCrB7beqCZzsLj8Ky3XSFhuo7QEbSJPjrCAiuf7SYC7bnnoSkELbba8JLs8TzNbBLzLhRKwUeihcyAJfnvMbB', 'seed':'brisk leave ship noble auto nasty gain expose crater friend deer cinnamon', 'address':'aULf8y', 'eth':'2', 'code':'EC0001', 'state':'이천시', 'type':'농촌', 'msg':'농사철 부족한 농촌 일손을 도와주기 위해 설립된 단체입니다.', 'cnt':'23023', 'thumb':'https://pbs.twimg.com/media/CgTnCGpUYAAC6EL.jpg'},
    {'t':'g', 'name':'EnEn', 'email':'admin@EnEn.kr', 'mobile':'031-222-3333', 'private':'xprv9yrSu6qtwAXEVhjn473VEsdTMLgfPUGE6Nihe1Dg5rsKEwcECxFZGfRbyuJjWU1gCgveVSHthPX4aDQsFYvXM767vfT6L2Gs1sm796nYwiY', 'seed':'tower lake window donkey point mango soul approve cement practice enlist lemon', 'address':'BKBvhD', 'eth':'100', 'code':'GP0001', 'state':'가평군', 'type':'환경', 'msg':'여행지에 버려진 쓰레기들을 치우는 활동을 합니다. Save Earth, Save Us', 'cnt':'231', 'thumb':'http://snvision.seongnam.go.kr/imgdata/snvision/201605/2016052337218339.jpg'},
    {'t':'g', 'name':'1석2조', 'email':'ilsuk2@gmail.com', 'mobile':'010-2435-2489', 'private':'xprv9xo9kgP3vryRA9haib5yKyE9yUBDYCJjStRjkgpu37z4AK8A7MnxC8kuxLq3nNemrXn4sBj4qpTCe7H2XuMvuurhgFnuf6ucn637E8HaYGq', 'seed':'shallow evolve dirt sense wire spare exist time bright deer access arrive', 'address':'111111', 'eth':'1000', 'code':'SWJA0005', 'state':'수원시 장안구', 'type':'개발', 'msg':'블록체인 해커톤에 참여하여 최우수상을 타고 돌아올 분들을 구합니다.', 'cnt':'5', 'thumb':'http://www.showmethetee.com/shop/goods/image/text/2017010212130118.jpg'},
    {'t':'g', 'name':'삼사삼사', 'email':'3434@goodthing.com', 'mobile':'031-3434-3434', 'private':'xprv9xmRUcbHYJFyUseS5pZG1zY9SM44JEVW3EzHnQfgffTGu51mozf7ZPLYSpciycLuT9B4HQY9JzaqiSSGfhjjssuT7HqVQ1m7MxNS1u5Copn', 'seed':'excuse domain ramp fault unhappy puzzle fence myself oak present produce soccer', 'address':'NlAHSM', 'eth':'34', 'code':'SWGS0100', 'state':'수원시 권선구', 'type':'돌봄', 'msg':'3분이서 4개의 가구의 독거 노인을 보살피는 활동입니다.', 'cnt':'3434', 'thumb':'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQZAMbQi5cPokQu4U0IjC68BnyKmfoBuDQUM0Aa2oD9XsikzevJEQ'},
    {'t':'g', 'name':'이재명 경기도 지사님의 법률상담소', 'email':'ljm@lawconsulting.com', 'mobile':'031-000-0000', 'private':'xprv9xmRUcbHYJFyUseS5pZG1zY9SM44JEVW3EzHnQfgffTGu51mozf7ZPLYSpciycLuT9B4HQY9JzaqiSSGfhjjssuT7HqVQ1m7MxNS1u5Copn', 'seed':'oyster message often excuse pattern penalty dad jaguar fabric ostrich east chapter', 'address':'Nbnm18', 'eth':'1000000', 'code':'SN0001', 'state':'성남시', 'type':'시설', 'msg':'이재명 경기도 지사님의 무료 법률 상담 봉사활동 개최시 필요한 시설물 보조 관리 인력 모집합니다.', 'cnt':'111111', 'thumb':'https://pbs.twimg.com/profile_images/978253833389228032/wHax6iwo_400x400.jpg'},
    {'t':'g', 'name':'수정공원 사랑이', 'email':'crystal@park.com', 'mobile':'031-111-1234', 'private':'xprv9ximoAc1uyfEM3DfXStssoTF4bfWLPXfo6gdBskZjb4Xd628c5HZPraW4Vj2k11BSRCRNdBmArTywnudcac1LjfLGY6wuR4EPvEzPeazcK9', 'seed':'page pledge deer believe match pledge throw ocean snap replace identify easy', 'address':'Vxmftm', 'eth':'200', 'code':'HW0001', 'state':'화성시', 'type':'환경', 'msg':'화성시 공원청소 하실 인원 구합니다.', 'cnt':'13', 'thumb':'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSlBSj1KO9UPrONmTEmRwlBdA1gmtnuQWEXeXDX-tN-J0WizihD'},
    {'t':'g', 'name':'금 공원 지키미', 'email':'gold@park.com', 'mobile':'031-222-1234', 'private':'xprv9yCBsFBKqjftbfbZTnX45Y8WTCqg3R1C3KEvELDbdicShsMmZ8MaaosvWaqtPYLo1ovUs9REYDgeVU3Kyp7dzk29V3ipP8qQ3UheZ1WoNRm', 'seed':'cable defense acid begin exchange charge peanut warfare evolve virus corn nation', 'address':'asVgp2', 'eth':'22', 'code':'BC0001', 'state':'부천시', 'type':'환경', 'msg':'부천시 공원청소 하실 인원 구합니다.', 'cnt':'20', 'thumb':'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQo2QU0VnqB9qn1xFDyqBkd6E_cvcC6bt7zHPzfLsmQcfuJnCC8'},
]

events = [
    {'code':'SWJA7777', 'eid':'N1e6EzSe', 'startdate':'20180727', 'enddate':'20180829', 'rnum':'5', 'rdnum':'5', 'state':'수원시 장안구', 'activity':'2018년 경기도 블록체인 해커톤 참가 및 우승', 'note':'1등은 우리의 것', 'isactive':'false'},
    {'code':'EW0001', 'eid':'iLfW6TmI', 'startdate':'20180812', 'enddate':'20180829', 'rnum':'14', 'rdnum':'3', 'state':'의왕시', 'activity':'중학교 1학년 대상 수학 교육', 'note':'최소 4시간, 최대 8시간 소요', 'isactive':'true'},
    {'code':'EW0001', 'eid':'HwZw6Km4', 'startdate':'20180812', 'enddate':'20180829', 'rnum':'12', 'rdnum':'5', 'state':'의왕시', 'activity':'중학교 2학년 대상 수학 교육', 'note':'최소 4시간, 최대 8시간 소요', 'isactive':'true'},
    {'code':'EW0001', 'eid':'qHjmlXVY', 'startdate':'20180301', 'enddate':'20180629', 'rnum':'17', 'rdnum':'17', 'state':'의왕시', 'activity':'중학교 1학년 대상 수학 교육', 'note':'최소 4시간, 최대 8시간 소요', 'isactive':'false'},
    {'code':'EW0001', 'eid':'BCILU9St', 'startdate':'20180301', 'enddate':'20180629', 'rnum':'15', 'rdnum':'15', 'state':'의왕시', 'activity':'중학교 2학년 대상 수학 교육', 'note':'최소 4시간, 최대 8시간 소요', 'isactive':'false'},
    {'code':'OS0001', 'eid':'NOcdixSF', 'startdate':'20180701', 'enddate':'20180731', 'rnum':'5', 'rdnum':'4', 'state':'오산시', 'activity':'무료 이발 봉사', 'note':'인원수 상관없이 목표 시간 달성 시 종료', 'isactive':'true'},
    {'code':'SWPD0121', 'eid':'vhaoePwt', 'startdate':'20180601', 'enddate':'20180601', 'rnum':'10', 'rdnum':'10', 'state':'수원시 팔달구', 'activity':'노인 목욕', 'note':'주기적으로 와주실 수 있는 분이면 더 좋습니다.', 'isactive':'false'},
    {'code':'SWPD0121', 'eid':'nP2uotTe', 'startdate':'20180615', 'enddate':'20180615', 'rnum':'10', 'rdnum':'10', 'state':'수원시 팔달구', 'activity':'화장실 청소', 'note':'2인당 한 화장실 청소 예정', 'isactive':'false'},
    {'code':'SWPD0121', 'eid':'BmQAb1pp', 'startdate':'20180701', 'enddate':'20180701', 'rnum':'12', 'rdnum':'10', 'state':'수원시 팔달구', 'activity':'노인 목욕', 'note':'주기적으로 와주실 수 있는 분이면 더 좋습니다.', 'isactive':'false'},
    {'code':'SWPD0121', 'eid':'O9gE3wec', 'startdate':'20180715', 'enddate':'20180715', 'rnum':'10', 'rdnum':'10', 'state':'수원시 팔달구', 'activity':'화장실 청소', 'note':'2인당 한 화장실 청소 예정', 'isactive':'false'},
    {'code':'SWPD0121', 'eid':'TlP875wM', 'startdate':'20180801', 'enddate':'20180801', 'rnum':'12', 'rdnum':'12', 'state':'수원시 팔달구', 'activity':'노인 목욕', 'note':'주기적으로 와주실 수 있는 분이면 더 좋습니다.', 'isactive':'false'},
    {'code':'SWPD0121', 'eid':'BmQAb1pp', 'startdate':'20180815', 'enddate':'20180815', 'rnum':'10', 'rdnum':'10', 'state':'수원시 팔달구', 'activity':'화장실 청소', 'note':'2인당 한 화장실 청소 예정', 'isactive':'false'},
    {'code':'SWPD0121', 'eid':'B1HRxORI', 'startdate':'20180901', 'enddate':'20180901', 'rnum':'10', 'rdnum':'3', 'state':'수원시 팔달구', 'activity':'노인 목욕', 'note':'주기적으로 와주실 수 있는 분이면 더 좋습니다.', 'isactive':'true'},
    {'code':'SWJA0001', 'eid':'flMnsr9k', 'startdate':'20180601', 'enddate':'20181231', 'rnum':'20', 'rdnum':'20', 'state':'수원시 장안구', 'activity':'병원 주요 시설 청소', 'note':'매 주 일요일 오후 1시부터 3시까지 실시합니다.', 'isactive':'false'},
    {'code':'EC0001', 'eid':'4q4atkuk', 'startdate':'20180310', 'enddate':'20180310', 'rnum':'10', 'rdnum':'10', 'state':'이천시', 'activity':'농촌 봉사 활동', 'note':'개인별 수건 준비', 'isactive':'false'},
    {'code':'EC0001', 'eid':'kl8027hl', 'startdate':'20180315', 'enddate':'20180315', 'rnum':'10', 'rdnum':'10', 'state':'이천시', 'activity':'농촌 봉사 활동', 'note':'개인별 수건 준비', 'isactive':'false'},
    {'code':'EC0001', 'eid':'2kuh0u8g', 'startdate':'20180330', 'enddate':'20180330', 'rnum':'10', 'rdnum':'10', 'state':'이천시', 'activity':'농촌 봉사 활동', 'note':'개인별 수건 준비', 'isactive':'false'},
    {'code':'EC0001', 'eid':'h5tc8mvh', 'startdate':'20180410', 'enddate':'20180410', 'rnum':'10', 'rdnum':'10', 'state':'이천시', 'activity':'농촌 봉사 활동', 'note':'개인별 수건 준비', 'isactive':'false'},
    {'code':'EC0001', 'eid':'65r6u4pk', 'startdate':'20180415', 'enddate':'20180415', 'rnum':'10', 'rdnum':'10', 'state':'이천시', 'activity':'농촌 봉사 활동', 'note':'개인별 수건 준비', 'isactive':'false'},
    {'code':'EC0001', 'eid':'i65kj5oi', 'startdate':'20180430', 'enddate':'20180430', 'rnum':'10', 'rdnum':'10', 'state':'이천시', 'activity':'농촌 봉사 활동', 'note':'개인별 수건 준비', 'isactive':'false'},
    {'code':'EC0001', 'eid':'7px6dgap', 'startdate':'20180510', 'enddate':'20180510', 'rnum':'10', 'rdnum':'10', 'state':'이천시', 'activity':'농촌 봉사 활동', 'note':'개인별 수건 준비', 'isactive':'false'},
    {'code':'EC0001', 'eid':'gt3mxuun', 'startdate':'20180515', 'enddate':'20180515', 'rnum':'10', 'rdnum':'10', 'state':'이천시', 'activity':'농촌 봉사 활동', 'note':'개인별 수건 준비', 'isactive':'false'},
    {'code':'EC0001', 'eid':'0l3cma07', 'startdate':'20180530', 'enddate':'20180530', 'rnum':'10', 'rdnum':'10', 'state':'이천시', 'activity':'농촌 봉사 활동', 'note':'개인별 수건 준비', 'isactive':'false'},
    {'code':'EC0001', 'eid':'rocknkc2', 'startdate':'20180610', 'enddate':'20180610', 'rnum':'10', 'rdnum':'10', 'state':'이천시', 'activity':'농촌 봉사 활동', 'note':'개인별 수건 준비', 'isactive':'false'},
    {'code':'EC0001', 'eid':'qgh1zbse', 'startdate':'20180615', 'enddate':'20180615', 'rnum':'10', 'rdnum':'10', 'state':'이천시', 'activity':'농촌 봉사 활동', 'note':'개인별 수건 준비', 'isactive':'false'},
    {'code':'EC0001', 'eid':'wysx7bmt', 'startdate':'20180630', 'enddate':'20180630', 'rnum':'10', 'rdnum':'10', 'state':'이천시', 'activity':'농촌 봉사 활동', 'note':'개인별 수건 준비', 'isactive':'false'},
    {'code':'EC0001', 'eid':'uc68f1h9', 'startdate':'20180710', 'enddate':'20180710', 'rnum':'10', 'rdnum':'10', 'state':'이천시', 'activity':'농촌 봉사 활동', 'note':'개인별 수건 준비', 'isactive':'false'},
    {'code':'EC0001', 'eid':'m2i24rdy', 'startdate':'20180715', 'enddate':'20180715', 'rnum':'10', 'rdnum':'10', 'state':'이천시', 'activity':'농촌 봉사 활동', 'note':'개인별 수건 준비', 'isactive':'false'},
    {'code':'EC0001', 'eid':'m2i24rdy', 'startdate':'20180730', 'enddate':'20180730', 'rnum':'10', 'rdnum':'10', 'state':'이천시', 'activity':'농촌 봉사 활동', 'note':'개인별 수건 준비', 'isactive':'false'},
    {'code':'EC0001', 'eid':'m2i24rdy', 'startdate':'20180810', 'enddate':'20180810', 'rnum':'10', 'rdnum':'10', 'state':'이천시', 'activity':'농촌 봉사 활동', 'note':'개인별 수건 준비', 'isactive':'false'},
    {'code':'EC0001', 'eid':'m2i24rdy', 'startdate':'20180815', 'enddate':'20180815', 'rnum':'10', 'rdnum':'10', 'state':'이천시', 'activity':'농촌 봉사 활동', 'note':'개인별 수건 준비', 'isactive':'false'},
    {'code':'EC0001', 'eid':'m2i24rdy', 'startdate':'20180830', 'enddate':'20180830', 'rnum':'10', 'rdnum':'9', 'state':'이천시', 'activity':'농촌 봉사 활동', 'note':'개인별 수건 준비', 'isactive':'true'},
    {'code':'GP0001', 'eid':'rocknkc2', 'startdate':'20180612', 'enddate':'20180612', 'rnum':'20', 'rdnum':'20', 'state':'가평군', 'activity':'쓰레기수거', 'note':'썬크림', 'isactive':'false'},
    {'code':'GP0001', 'eid':'WJuU82zr', 'startdate':'20180628', 'enddate':'20180628', 'rnum':'20', 'rdnum':'20', 'state':'가평군', 'activity':'쓰레기수거', 'note':'썬크림', 'isactive':'false'},
    {'code':'GP0001', 'eid':'dbeivCCZ', 'startdate':'20180701', 'enddate':'20180701', 'rnum':'20', 'rdnum':'20', 'state':'가평군', 'activity':'쓰레기수거', 'note':'썬크림', 'isactive':'false'},
    {'code':'GP0001', 'eid':'nR7v52rl', 'startdate':'20180711', 'enddate':'20180711', 'rnum':'20', 'rdnum':'20', 'state':'가평군', 'activity':'쓰레기수거', 'note':'썬크림', 'isactive':'false'},
    {'code':'GP0001', 'eid':'UuOi0hJJ', 'startdate':'20180725', 'enddate':'20180725', 'rnum':'20', 'rdnum':'20', 'state':'가평군', 'activity':'쓰레기수거', 'note':'썬크림', 'isactive':'false'},
    {'code':'GP0001', 'eid':'KfogRC0V', 'startdate':'20180801', 'enddate':'20180801', 'rnum':'20', 'rdnum':'20', 'state':'가평군', 'activity':'쓰레기수거', 'note':'썬크림', 'isactive':'false'},
    {'code':'GP0001', 'eid':'qEpXs5se', 'startdate':'20180809', 'enddate':'20180809', 'rnum':'20', 'rdnum':'20', 'state':'가평군', 'activity':'쓰레기수거', 'note':'썬크림', 'isactive':'false'},
    {'code':'GP0001', 'eid':'lLV7i8ZF', 'startdate':'20180819', 'enddate':'20180819', 'rnum':'20', 'rdnum':'20', 'state':'가평군', 'activity':'쓰레기수거', 'note':'썬크림', 'isactive':'false'},
    {'code':'GP0001', 'eid':'EHbIFmmE', 'startdate':'20180822', 'enddate':'20180822', 'rnum':'20', 'rdnum':'20', 'state':'가평군', 'activity':'쓰레기수거', 'note':'썬크림', 'isactive':'false'},
    {'code':'GP0001', 'eid':'LGt8as1P', 'startdate':'20180905', 'enddate':'20180905', 'rnum':'20', 'rdnum':'13', 'state':'가평군', 'activity':'쓰레기수거', 'note':'썬크림', 'isactive':'true'},
    {'code':'SWJA0005', 'eid':'uRCVV0OW', 'startdate':'20180829', 'enddate':'20180829', 'rnum':'5', 'rdnum':'5', 'state':'수원시 장안구', 'activity':'개발', 'note':'노트북', 'isactive':'false'},
    {'code':'SWJA0005', 'eid':'DgpqPkZB', 'startdate':'20180827', 'enddate':'20180827', 'rnum':'5', 'rdnum':'5', 'state':'수원시 장안구', 'activity':'개발', 'note':'노트북', 'isactive':'false'},
    {'code':'SWJA0005', 'eid':'Alh8wDyf', 'startdate':'20180825', 'enddate':'20180825', 'rnum':'5', 'rdnum':'5', 'state':'수원시 장안구', 'activity':'개발', 'note':'노트북', 'isactive':'false'},
    {'code':'SWJA0005', 'eid':'Yx6ScbH6', 'startdate':'20180822', 'enddate':'20180822', 'rnum':'5', 'rdnum':'5', 'state':'수원시 장안구', 'activity':'개발', 'note':'노트북', 'isactive':'false'},
    {'code':'SWJA0005', 'eid':'f5pfNQNt', 'startdate':'20180820', 'enddate':'20180820', 'rnum':'5', 'rdnum':'5', 'state':'수원시 장안구', 'activity':'개발', 'note':'노트북', 'isactive':'false'},
    {'code':'SWJA0005', 'eid':'yQAnHCVd', 'startdate':'20180819', 'enddate':'20180819', 'rnum':'5', 'rdnum':'5', 'state':'수원시 장안구', 'activity':'개발', 'note':'노트북', 'isactive':'false'},
    {'code':'SWJA0005', 'eid':'XAfqXc1e', 'startdate':'20180815', 'enddate':'20180815', 'rnum':'5', 'rdnum':'5', 'state':'수원시 장안구', 'activity':'개발', 'note':'노트북', 'isactive':'false'},
    {'code':'SWJA0005', 'eid':'hK5krrfW', 'startdate':'20180813', 'enddate':'20180813', 'rnum':'5', 'rdnum':'5', 'state':'수원시 장안구', 'activity':'개발', 'note':'노트북', 'isactive':'false'},
    {'code':'SWJA0005', 'eid':'NRqwqHP3', 'startdate':'20180811', 'enddate':'20180811', 'rnum':'5', 'rdnum':'5', 'state':'수원시 장안구', 'activity':'개발', 'note':'노트북', 'isactive':'false'},
    {'code':'SWJA0005', 'eid':'wDZpfp3B', 'startdate':'20180801', 'enddate':'20180801', 'rnum':'5', 'rdnum':'5', 'state':'수원시 장안구', 'activity':'개발', 'note':'노트북', 'isactive':'false'},
    {'code':'SWGS0100', 'eid':'qQhxp3y1', 'startdate':'20180501', 'enddate':'20180501', 'rnum':'3', 'rdnum':'3', 'state':'수원시 장안구', 'activity':'노인', 'note':'열심히 합시다', 'isactive':'false'},
    {'code':'SWGS0100', 'eid':'mY3mb4tx', 'startdate':'20180515', 'enddate':'20180515', 'rnum':'3', 'rdnum':'3', 'state':'수원시 장안구', 'activity':'노인', 'note':'열심히 합시다', 'isactive':'false'},
    {'code':'SWGS0100', 'eid':'x8WGVXQ8', 'startdate':'20180601', 'enddate':'20180601', 'rnum':'3', 'rdnum':'3', 'state':'수원시 장안구', 'activity':'노인', 'note':'열심히 합시다', 'isactive':'false'},
    {'code':'SWGS0100', 'eid':'7r9em0F4', 'startdate':'20180615', 'enddate':'20180615', 'rnum':'3', 'rdnum':'3', 'state':'수원시 장안구', 'activity':'노인', 'note':'열심히 합시다', 'isactive':'false'},
    {'code':'SWGS0100', 'eid':'nkR7RzeE', 'startdate':'20180701', 'enddate':'20180701', 'rnum':'3', 'rdnum':'3', 'state':'수원시 장안구', 'activity':'노인', 'note':'열심히 합시다', 'isactive':'false'},
    {'code':'SWGS0100', 'eid':'hPhYOSwC', 'startdate':'20180715', 'enddate':'20180715', 'rnum':'3', 'rdnum':'3', 'state':'수원시 장안구', 'activity':'노인', 'note':'열심히 합시다', 'isactive':'false'},
    {'code':'SWGS0100', 'eid':'AGgKn5Xr', 'startdate':'20180801', 'enddate':'20180801', 'rnum':'3', 'rdnum':'3', 'state':'수원시 장안구', 'activity':'노인', 'note':'열심히 합시다', 'isactive':'false'},
    {'code':'SWGS0100', 'eid':'iZBBNdiU', 'startdate':'20180815', 'enddate':'20180815', 'rnum':'3', 'rdnum':'3', 'state':'수원시 장안구', 'activity':'노인', 'note':'열심히 합시다', 'isactive':'false'},
    {'code':'SWGS0100', 'eid':'lRFCwwjD', 'startdate':'20180901', 'enddate':'20180901', 'rnum':'3', 'rdnum':'3', 'state':'수원시 장안구', 'activity':'노인', 'note':'열심히 합시다', 'isactive':'false'},
    {'code':'SWGS0100', 'eid':'QdEZhx80', 'startdate':'20180915', 'enddate':'20180915', 'rnum':'3', 'rdnum':'1', 'state':'수원시 장안구', 'activity':'노인', 'note':'열심히 합시다', 'isactive':'true'},
    {'code':'PC0001', 'eid':'Rv5xJxL0', 'startdate':'20180322', 'enddate':'20180323', 'rnum':'50', 'rdnum':'50', 'state':'평창군', 'activity':'시설관리', 'note':'기본적인 유지를 위한 일입니다', 'isactive':'false'},
    {'code':'PC0001', 'eid':'DLJdZGdt', 'startdate':'20180422', 'enddate':'20180423', 'rnum':'50', 'rdnum':'50', 'state':'평창군', 'activity':'시설관리', 'note':'기본적인 유지를 위한 일입니다', 'isactive':'false'},
    {'code':'PC0001', 'eid':'tjV7ldVt', 'startdate':'20180522', 'enddate':'20180523', 'rnum':'50', 'rdnum':'50', 'state':'평창군', 'activity':'시설관리', 'note':'기본적인 유지를 위한 일입니다', 'isactive':'false'},
    {'code':'PC0001', 'eid':'GZuVDpt6', 'startdate':'20180622', 'enddate':'20180623', 'rnum':'50', 'rdnum':'50', 'state':'평창군', 'activity':'시설관리', 'note':'기본적인 유지를 위한 일입니다', 'isactive':'false'},
    {'code':'PC0001', 'eid':'TbbPhwCe', 'startdate':'20180722', 'enddate':'20180723', 'rnum':'50', 'rdnum':'50', 'state':'평창군', 'activity':'시설관리', 'note':'기본적인 유지를 위한 일입니다', 'isactive':'false'},
    {'code':'PC0001', 'eid':'YZmy2XZ1', 'startdate':'20180822', 'enddate':'20180823', 'rnum':'50', 'rdnum':'50', 'state':'평창군', 'activity':'시설관리', 'note':'기본적인 유지를 위한 일입니다', 'isactive':'false'},
    {'code':'GP0001', 'eid':'AwlSjLsc', 'startdate':'20180524', 'enddate':'20180524', 'rnum':'20', 'rdnum':'20', 'state':'군포시', 'activity':'노인돌봄', 'note':'어르신들이 매우 좋으세요', 'isactive':'false'},
    {'code':'GP0001', 'eid':'xFqz0CjV', 'startdate':'20180624', 'enddate':'20180624', 'rnum':'20', 'rdnum':'20', 'state':'군포시', 'activity':'노인돌봄', 'note':'어르신들이 매우 좋으세요', 'isactive':'false'},
    {'code':'GP0001', 'eid':'RG8yfH3S', 'startdate':'20180724', 'enddate':'20180724', 'rnum':'20', 'rdnum':'20', 'state':'군포시', 'activity':'노인돌봄', 'note':'어르신들이 매우 좋으세요', 'isactive':'false'},
    {'code':'GP0001', 'eid':'9h4A5PoK', 'startdate':'20180824', 'enddate':'20180824', 'rnum':'20', 'rdnum':'20', 'state':'군포시', 'activity':'노인돌봄', 'note':'어르신들이 매우 좋으세요', 'isactive':'false'},
    {'code':'GP0001', 'eid':'YHCNWQZ5', 'startdate':'20180924', 'enddate':'20180924', 'rnum':'20', 'rdnum':'19', 'state':'군포시', 'activity':'노인돌봄', 'note':'어르신들이 매우 좋으세요', 'isactive':'true'},
    {'code':'GP0001', 'eid':'NWDAieX5', 'startdate':'20181024', 'enddate':'20181024', 'rnum':'20', 'rdnum':'6', 'state':'군포시', 'activity':'노인돌봄', 'note':'어르신들이 매우 좋으세요', 'isactive':'true'},
    {'code':'BC0001', 'eid':'2nHldUbZ', 'startdate':'20180624', 'enddate':'20180624', 'rnum':'8', 'rdnum':'8', 'state':'부천시', 'activity':'환경정화', 'note':'별로 안 어려워요', 'isactive':'false'},
    {'code':'BC0001', 'eid':'IQx5mopM', 'startdate':'20180702', 'enddate':'20180702', 'rnum':'8', 'rdnum':'8', 'state':'부천시', 'activity':'환경정화', 'note':'별로 안 어려워요', 'isactive':'false'},
    {'code':'BC0001', 'eid':'b0l0rkq4', 'startdate':'20180713', 'enddate':'20180713', 'rnum':'8', 'rdnum':'8', 'state':'부천시', 'activity':'환경정화', 'note':'별로 안 어려워요', 'isactive':'false'},
    {'code':'BC0001', 'eid':'3SzAjCKG', 'startdate':'20180726', 'enddate':'20180726', 'rnum':'8', 'rdnum':'8', 'state':'부천시', 'activity':'환경정화', 'note':'별로 안 어려워요', 'isactive':'false'},
    {'code':'BC0001', 'eid':'DSYIAlKC', 'startdate':'20180814', 'enddate':'20180814', 'rnum':'8', 'rdnum':'8', 'state':'부천시', 'activity':'환경정화', 'note':'별로 안 어려워요', 'isactive':'false'},
    {'code':'BC0001', 'eid':'NBHym9nv', 'startdate':'20180825', 'enddate':'20180825', 'rnum':'8', 'rdnum':'8', 'state':'부천시', 'activity':'환경정화', 'note':'별로 안 어려워요', 'isactive':'false'},
    {'code':'HW0001', 'eid':'HnVm2kF4', 'startdate':'20180624', 'enddate':'20180624', 'rnum':'10', 'rdnum':'10', 'state':'화성시', 'activity':'환경정화', 'note':'조금 덥습니다.', 'isactive':'false'},
    {'code':'HW0001', 'eid':'uHRaduka', 'startdate':'20180701', 'enddate':'20180701', 'rnum':'10', 'rdnum':'10', 'state':'화성시', 'activity':'환경정화', 'note':'조금 덥습니다.', 'isactive':'false'},
    {'code':'HW0001', 'eid':'YiXbnkdH', 'startdate':'20180720', 'enddate':'20180720', 'rnum':'10', 'rdnum':'10', 'state':'화성시', 'activity':'환경정화', 'note':'조금 덥습니다.', 'isactive':'false'},
    {'code':'HW0001', 'eid':'SPmrW614', 'startdate':'20180801', 'enddate':'20180801', 'rnum':'10', 'rdnum':'10', 'state':'화성시', 'activity':'환경정화', 'note':'조금 덥습니다.', 'isactive':'false'},
    {'code':'HW0001', 'eid':'X5Uc7FAc', 'startdate':'20180820', 'enddate':'20180820', 'rnum':'10', 'rdnum':'10', 'state':'화성시', 'activity':'환경정화', 'note':'조금 덥습니다.', 'isactive':'false'},
    {'code':'HW0001', 'eid':'9Wl0pb6U', 'startdate':'20180903', 'enddate':'20180903', 'rnum':'10', 'rdnum':'8', 'state':'화성시', 'activity':'환경정화', 'note':'조금 덥습니다.', 'isactive':'true'},
    {'code':'SN0001', 'eid':'RnuZp3oM', 'startdate':'20180915', 'enddate':'20180915', 'rnum':'3', 'rdnum':'1', 'state':'성남시', 'activity':'시설관리', 'note':'힘이 좋으신 분', 'isactive':'true'},
    {'code':'SN0001', 'eid':'jTs5FAYm', 'startdate':'20180819', 'enddate':'20180819', 'rnum':'3', 'rdnum':'3', 'state':'성남시', 'activity':'시설관리', 'note':'힘이 좋으신 분', 'isactive':'false'},
    {'code':'SN0001', 'eid':'cSP3Q7R0', 'startdate':'20180802', 'enddate':'20180802', 'rnum':'3', 'rdnum':'3', 'state':'성남시', 'activity':'시설관리', 'note':'힘이 좋으신 분', 'isactive':'false'},
    {'code':'SN0001', 'eid':'KVTkIQXG', 'startdate':'20180720', 'enddate':'20180720', 'rnum':'3', 'rdnum':'3', 'state':'성남시', 'activity':'시설관리', 'note':'힘이 좋으신 분', 'isactive':'false'},
    {'code':'SN0001', 'eid':'sQ9FlWTf', 'startdate':'20180706', 'enddate':'20180706', 'rnum':'3', 'rdnum':'3', 'state':'성남시', 'activity':'시설관리', 'note':'힘이 좋으신 분', 'isactive':'false'},
    {'code':'SN0001', 'eid':'12Ught7C', 'startdate':'20180624', 'enddate':'20180624', 'rnum':'3', 'rdnum':'3', 'state':'성남시', 'activity':'시설관리', 'note':'힘이 좋으신 분', 'isactive':'false'},
]
'''
comments = [
    {'code':'EW0001','name':'심건영','comment':'해커톤 1등하고 싶어요'},
    {'code':'SWPD0121','name':'길라임','comment':'해커톤 1등하면 좋아요'},
    {'code':'SWPD0121','name':'Dustin Nippert','comment':'우리가 해커톤 1등이라니!'},
    {'code':'SWPD0121','name':'한일석','comment':'기부도 하고 봉사도 하고'},
    {'code':'SWPD0121','name':'김동일','comment':'사용자 친화적 UI'},
    {'code':'SWJA0001','name':'이원희','comment':'투명한 봉사활동 관리'},
    {'code':'SWJA0001','name':'박성수','comment':'투명한 기부내역 확인'},
    {'code':'SWJA0001','name':'이승진','comment':'투명한 봉사단체 후기작성 및 확인'},
    {'code':'SWJA0001','name':'이재현','comment':'편리한 기부 방식'},
    {'code':'SWJA7777','name':'심건영','comment':'즐거운 봉사활동 해요'},
    {'code':'OS0001','name':'길라임','comment':'투명한 봉사활동 관리'},
    {'code':'OS0001','name':'한일석','comment':'여기 관계자분 디지털티 입고 깎아주시는데 정말 잘 깎으시는 것 같아요. 역시 군인'}
]'''
comments = [
    {'code': 'EW0001', 'name': '심건영', 'address': 'blinij9', 'level': '1', 'comment': '해커톤 1등하고 싶어요'},
    {'code': 'SWPD0121', 'name': '한일석', 'address': 'a2v2dh5', 'level': '2', 'comment': '해커톤 1등하면 좋아요'},
    {'code': 'SWPD0121', 'name': 'Dustin Nippert', 'address': '37kp53b', 'level': '1', 'comment': '우리가 해커톤 1등이라니!'},
    {'code': 'SWPD0121', 'name': '길라임', 'address': '1nglrp8', 'level': '1', 'comment': '기부도 하고 봉사도 하고'},
    {'code': 'SWPD0121', 'name': '김동일', 'address': 'g14gdsf', 'level': '2', 'comment': '사용자 친화적 UI'},
    {'code': 'SWJA0001', 'name': '이원희', 'address': 'u35o2jf', 'level': '3', 'comment': '투명한 봉사활동 관리'},
    {'code': 'SWJA0001', 'name': '심건영', 'address': 'blinij9', 'level': '1', 'comment': '투명한 기부내역 확인'},
    {'code': 'SWJA0001', 'name': '길라임', 'address': '1nglrp8', 'level': '1', 'comment': '투명한 봉사단체 후기작성 및 확인'},
    {'code': 'SWJA0001', 'name': 'Dustin Nippert', 'address': '37kp53b', 'level': '1', 'comment': '편리한 기부 방식'},
    {'code': 'SWJA7777', 'name': '한일석', 'address': 'a2v2dh5', 'level': '2', 'comment': '즐거운 봉사활동 해요'},
    {'code': 'OS0001', 'name': '김동일', 'address': 'g14gdsf', 'level': '2', 'comment': '투명한 봉사활동 관리'},
    {'code': 'OS0001', 'name': '이원희', 'address': 'u35o2jf', 'level': '3', 'comment': '여기 관계자분 디지털티 입고 깎아주시는데 정말 잘 깎으시는 것 같아요. 역시 군인'},
    {'code':'GP0001','name':'익명의 고라니','address':'nk6i630','level':'2','comment':'힘들지만 보람찹니다.'},
    {'code':'GP0001','name':'익명의 고라니','address':'o0ln4wo', 'level':'1','comment':'힘든 하루였지만 보람되었습니다.'},
    {'code':'GP0001','name':'익명의 고라니','address':'ks993gg','level':'5','comment':'기부가 시급합니다. 돈이 없어요.'},
    {'code':'GP0001', 'name':'익명의 고라니', 'address':'ks993gg', 'level':'3', 'comment':'기부자들이 많으면 좋겠네요. 돈이 부족합니다.'},
    {'code':'GP0001', 'name':'익명의 고라니', 'address':'g14gdsf', 'level':'2', 'comment':'단체가 하는 일보다 돈이 좀 부족한 것 같습니다.'},
    {'code':'GP0001', 'name':'익명의 고라니', 'address':'a2v2dh5', 'level':'2', 'comment':'이 단체에 돈 좀 주세요 ㅜㅜ'},
    {'code':'GP0001', 'name':'익명의 고라니', 'address':'37kp53b', 'level':'1', 'comment':'힘들어요 ㅜㅜ'},
    {'code':'GP0001', 'name':'익명의 고라니', 'address':'1nglrp8', 'level':'1', 'comment':'보람찹니다.'},
    {'code':'GP0001', 'name':'익명의 고라니', 'address':'blinij9', 'level':'1', 'comment':'굿'},
    {'code':'GP0001','name':'익명의 고라니','address':'nk6i630','level':'2','comment':'힘들지만 보람찹니다.'},
    {'code':'GP0001','name':'익명의 고라니','address':'o0ln4wo', 'level':'1','comment':'힘든 하루였지만 보람되었습니다.'},
    {'code':'GP0001','name':'익명의 고라니','address':'ks993gg','level':'5','comment':'기부가 시급합니다. 돈이 없어요.'},
    {'code':'GP0001', 'name':'익명의 고라니', 'address':'ks993gg', 'level':'3', 'comment':'기부자들이 많으면 좋겠네요. 돈이 부족합니다.'},
    {'code':'GP0001', 'name':'익명의 고라니', 'address':'g14gdsf', 'level':'2', 'comment':'단체가 하는 일보다 돈이 좀 부족한 것 같습니다.'},
    {'code':'GP0001', 'name':'익명의 고라니', 'address':'a2v2dh5', 'level':'2', 'comment':'이 단체에 돈 좀 주세요 ㅜㅜ'},
    {'code':'GP0001', 'name':'익명의 고라니', 'address':'37kp53b', 'level':'1', 'comment':'힘들어요 ㅜㅜ'},
    {'code':'GP0001', 'name':'익명의 고라니', 'address':'1nglrp8', 'level':'1', 'comment':'보람찹니다.'},
    {'code':'GP0001', 'name':'익명의 고라니', 'address':'blinij9', 'level':'1', 'comment':'굿'},
    {'code':'GP0001','name':'익명의 고라니','address':'nk6i630','level':'2','comment':'힘들지만 보람찹니다.'},
    {'code':'GP0001','name':'익명의 고라니','address':'o0ln4wo', 'level':'1','comment':'힘든 하루였지만 보람되었습니다.'},
    {'code':'GP0001','name':'익명의 고라니','address':'ks993gg','level':'5','comment':'기부가 시급합니다. 돈이 없어요.'},
    {'code':'GP0001', 'name':'익명의 고라니', 'address':'ks993gg', 'level':'3', 'comment':'기부자들이 많으면 좋겠네요. 돈이 부족합니다.'},
    {'code':'GP0001', 'name':'익명의 고라니', 'address':'g14gdsf', 'level':'2', 'comment':'단체가 하는 일보다 돈이 좀 부족한 것 같습니다.'},
    {'code':'GP0001', 'name':'익명의 고라니', 'address':'a2v2dh5', 'level':'2', 'comment':'이 단체에 돈 좀 주세요 ㅜㅜ'},
    {'code':'GP0001', 'name':'익명의 고라니', 'address':'37kp53b', 'level':'1', 'comment':'힘들어요 ㅜㅜ'},
    {'code':'GP0001', 'name':'익명의 고라니', 'address':'1nglrp8', 'level':'1', 'comment':'보람찹니다.'},
    {'code':'GP0001', 'name':'익명의 고라니', 'address':'blinij9', 'level':'1', 'comment':'굿'},
    {'code':'GP0001','name':'익명의 고라니','address':'nk6i630','level':'2','comment':'1등은 우리의 것'},
    {'code':'GP0001','name':'익명의 고라니','address':'o0ln4wo', 'level':'1','comment':'1등 주세요~'},
    {'code':'GP0001','name':'익명의 고라니','address':'ks993gg','level':'5','comment':'1등상 받고시포'},
    {'code':'GP0001', 'name':'익명의 고라니', 'address':'ks993gg', 'level':'3', 'comment':'1등 1등 1등 1등 1등'},
    {'code':'GP0001', 'name':'익명의 고라니', 'address':'g14gdsf', 'level':'2', 'comment':'1등만이 살길'},
    {'code':'GP0001', 'name':'익명의 고라니', 'address':'a2v2dh5', 'level':'2', 'comment':'1등만 기억하는 세상'},
    {'code':'GP0001', 'name':'익명의 고라니', 'address':'37kp53b', 'level':'1', 'comment':'1석이조 1등조'},
    {'code':'GP0001', 'name':'익명의 고라니', 'address':'1nglrp8', 'level':'1', 'comment':'1등 시켜주세요~'},
    {'code':'GP0001', 'name':'익명의 고라니', 'address':'blinij9', 'level':'1', 'comment':'1등짱'},
    {'code':'GP0001','name':'익명의 고라니','address':'nk6i630','level':'2','comment':'1등은 우리의 것'},
    {'code':'GP0001','name':'익명의 고라니','address':'o0ln4wo', 'level':'1','comment':'1등 주세요~'},
    {'code':'GP0001','name':'익명의 고라니','address':'ks993gg','level':'5','comment':'1등상 받고시포'},
    {'code':'GP0001', 'name':'익명의 고라니', 'address':'ks993gg', 'level':'3', 'comment':'1등 1등 1등 1등 1등'},
    {'code':'GP0001', 'name':'익명의 고라니', 'address':'g14gdsf', 'level':'2', 'comment':'1등만이 살길'},
    {'code':'GP0001', 'name':'익명의 고라니', 'address':'a2v2dh5', 'level':'2', 'comment':'1등만 기억하는 세상'},
    {'code':'GP0001', 'name':'익명의 고라니', 'address':'37kp53b', 'level':'1', 'comment':'1석이조 1등조'},
    {'code':'GP0001', 'name':'익명의 고라니', 'address':'1nglrp8', 'level':'1', 'comment':'1등 시켜주세요~'},
    {'code':'GP0001', 'name':'익명의 고라니', 'address':'blinij9', 'level':'1', 'comment':'1등짱'},
    {'code':'GP0001','name':'익명의 고라니','address':'nk6i630','level':'2','comment':'1등은 우리의 것'},
    {'code':'GP0001','name':'익명의 고라니','address':'o0ln4wo', 'level':'1','comment':'1등 주세요~'},
    {'code':'GP0001','name':'익명의 고라니','address':'ks993gg','level':'5','comment':'1등상 받고시포'},
    {'code':'GP0001', 'name':'익명의 고라니', 'address':'ks993gg', 'level':'3', 'comment':'1등 1등 1등 1등 1등'},
    {'code':'GP0001', 'name':'익명의 고라니', 'address':'g14gdsf', 'level':'2', 'comment':'1등만이 살길'},
    {'code':'GP0001', 'name':'익명의 고라니', 'address':'a2v2dh5', 'level':'2', 'comment':'1등만 기억하는 세상'},
    {'code':'GP0001', 'name':'익명의 고라니', 'address':'37kp53b', 'level':'1', 'comment':'1석이조 1등조'},
    {'code':'GP0001', 'name':'익명의 고라니', 'address':'1nglrp8', 'level':'1', 'comment':'1등 시켜주세요~'},
    {'code':'GP0001', 'name':'익명의 고라니', 'address':'blinij9', 'level':'1', 'comment':'1등짱'},
    {'code':'GP0001','name':'익명의 고라니','address':'nk6i630','level':'2','comment':'쓰레기 수거 힘들어요'},
    {'code':'GP0001','name':'익명의 고라니','address':'o0ln4wo', 'level':'1','comment':'냄새가 많이 나지만 보람 됩니다.'},
    {'code':'GP0001','name':'익명의 고라니','address':'ks993gg','level':'5','comment':'냄새가 고약하네요...'},
    {'code':'GP0001', 'name':'익명의 고라니', 'address':'ks993gg', 'level':'3', 'comment':'환경보호!'},
    {'code':'GP0001', 'name':'익명의 고라니', 'address':'g14gdsf', 'level':'2', 'comment':'냄새가 심합니다.'},
    {'code':'GP0001', 'name':'익명의 고라니', 'address':'a2v2dh5', 'level':'2', 'comment':'힘들어요 ㅜㅜㅜ'},
    {'code':'GP0001', 'name':'익명의 고라니', 'address':'37kp53b', 'level':'1', 'comment':'좀 더 잘 환경을 보존해야겠다는 생각이 듭니다.'},
    {'code':'GP0001', 'name':'익명의 고라니', 'address':'1nglrp8', 'level':'1', 'comment':'최고의 봉사활동이라 할 수 있습니다.'},
    {'code':'GP0001', 'name':'익명의 고라니', 'address':'blinij9', 'level':'1', 'comment':'별로네요… 힘들어요.'}, {'code':'GP0001','name':'익명의 고라니','address':'nk6i630','level':'2','comment':'쓰레기 수거 힘들어요'},
    {'code':'GP0001','name':'익명의 고라니','address':'o0ln4wo', 'level':'1','comment':'냄새가 많이 나지만 보람 됩니다.'},
    {'code':'GP0001','name':'익명의 고라니','address':'ks993gg','level':'5','comment':'냄새가 고약하네요...'},
    {'code':'GP0001', 'name':'익명의 고라니', 'address':'ks993gg', 'level':'3', 'comment':'환경보호!'},
    {'code':'GP0001', 'name':'익명의 고라니', 'address':'g14gdsf', 'level':'2', 'comment':'냄새가 심합니다.'},
    {'code':'GP0001', 'name':'익명의 고라니', 'address':'a2v2dh5', 'level':'2', 'comment':'힘들어요 ㅜㅜㅜ'},
    {'code':'GP0001', 'name':'익명의 고라니', 'address':'37kp53b', 'level':'1', 'comment':'좀 더 잘 환경을 보존해야겠다는 생각이 듭니다.'},
    {'code':'GP0001', 'name':'익명의 고라니', 'address':'1nglrp8', 'level':'1', 'comment':'최고의 봉사활동이라 할 수 있습니다.'},
    {'code':'GP0001', 'name':'익명의 고라니', 'address':'blinij9', 'level':'1', 'comment':'별로네요… 힘들어요.'}, {'code':'GP0001','name':'익명의 고라니','address':'nk6i630','level':'2','comment':'쓰레기 수거 힘들어요'},
    {'code':'GP0001','name':'익명의 고라니','address':'o0ln4wo', 'level':'1','comment':'냄새가 많이 나지만 보람 됩니다.'},
    {'code':'GP0001','name':'익명의 고라니','address':'ks993gg','level':'5','comment':'냄새가 고약하네요...'},
    {'code':'GP0001', 'name':'익명의 고라니', 'address':'ks993gg', 'level':'3', 'comment':'환경보호!'},
    {'code':'GP0001', 'name':'익명의 고라니', 'address':'g14gdsf', 'level':'2', 'comment':'냄새가 심합니다.'},
    {'code':'GP0001', 'name':'익명의 고라니', 'address':'a2v2dh5', 'level':'2', 'comment':'힘들어요 ㅜㅜㅜ'},
    {'code':'GP0001', 'name':'익명의 고라니', 'address':'37kp53b', 'level':'1', 'comment':'좀 더 잘 환경을 보존해야겠다는 생각이 듭니다.'},
    {'code':'GP0001', 'name':'익명의 고라니', 'address':'1nglrp8', 'level':'1', 'comment':'최고의 봉사활동이라 할 수 있습니다.'},
    {'code':'GP0001', 'name':'익명의 고라니', 'address':'blinij9', 'level':'1', 'comment':'별로네요… 힘들어요.'},
    {'code':'EC0001','name':'이재현','address':'nk6i630','level':'2','comment':'농촌 봉사활동 정말 최고입니다.'},
    {'code':'EC0001','name':'이승진','address':'o0ln4wo', 'level':'1','comment':'힘들지만 보람찬 하루가 되었습니다.'},
    {'code':'EC0001','name':'박성수','address':'ks993gg','level':'5','comment':'밥먹기까지 이렇게 힘든 노력이 있는지 처음 알았습니다. 어르신들 많이 도와드리는 이런 활동 꼭 필요하다고 생각합니다. 다만 식사도 단체에서 지원될 수 있게 기부가 많이 필요합니다.'},
    {'code':'EC0001', 'name':'익명의 고라니', 'address':'ks993gg', 'level':'3', 'comment':'농활짱짱'},
    {'code':'EC0001', 'name':'익명의 고라니', 'address':'g14gdsf', 'level':'2', 'comment':'여친사귈 수 있어요!'},
    {'code':'EC0001', 'name':'익명의 고라니', 'address':'a2v2dh5', 'level':'2', 'comment':'헤헤 재밌었다.'},
    {'code':'EC0001', 'name':'익명의 고라니', 'address':'37kp53b', 'level':'1', 'comment':'최고입니다.'},
    {'code':'EC0001', 'name':'익명의 고라니', 'address':'1nglrp8', 'level':'1', 'comment':'최고의 봉사활동이라 할 수 있습니다.'},
    {'code':'EC0001', 'name':'익명의 고라니', 'address':'blinij9', 'level':'1', 'comment':'별로네요… 힘들어요.'},
    {'code':'EC0001','name':'이재현','address':'nk6i630','level':'2','comment':'농촌 봉사활동 정말 최고입니다.'},
    {'code':'EC0001','name':'이승진','address':'o0ln4wo', 'level':'1','comment':'힘들지만 보람찬 하루가 되었습니다.'},
    {'code':'EC0001','name':'박성수','address':'ks993gg','level':'5','comment':'밥먹기까지 이렇게 힘든 노력이 있는지 처음 알았습니다. 어르신들 많이 도와드리는 이런 활동 꼭 필요하다고 생각합니다. 다만 식사도 단체에서 지원될 수 있게 기부가 많이 필요합니다.'},
    {'code':'EC0001', 'name':'익명의 고라니', 'address':'ks993gg', 'level':'3', 'comment':'농활짱짱'},
    {'code':'EC0001', 'name':'익명의 고라니', 'address':'g14gdsf', 'level':'2', 'comment':'여친사귈 수 있어요!'},
    {'code':'EC0001', 'name':'익명의 고라니', 'address':'a2v2dh5', 'level':'2', 'comment':'헤헤 재밌었다.'},
    {'code':'EC0001', 'name':'익명의 고라니', 'address':'37kp53b', 'level':'1', 'comment':'최고입니다.'},
    {'code':'EC0001', 'name':'익명의 고라니', 'address':'1nglrp8', 'level':'1', 'comment':'최고의 봉사활동이라 할 수 있습니다.'},
    {'code':'EC0001', 'name':'익명의 고라니', 'address':'blinij9', 'level':'1', 'comment':'별로네요… 힘들어요.'},
    {'code':'EC0001','name':'이재현','address':'nk6i630','level':'2','comment':'농촌 봉사활동 정말 최고입니다.'},
    {'code':'EC0001','name':'이승진','address':'o0ln4wo', 'level':'1','comment':'힘들지만 보람찬 하루가 되었습니다.'},
    {'code':'EC0001','name':'박성수','address':'ks993gg','level':'5','comment':'밥먹기까지 이렇게 힘든 노력이 있는지 처음 알았습니다. 어르신들 많이 도와드리는 이런 활동 꼭 필요하다고 생각합니다. 다만 식사도 단체에서 지원될 수 있게 기부가 많이 필요합니다.'},
    {'code':'EC0001', 'name':'익명의 고라니', 'address':'ks993gg', 'level':'3', 'comment':'농활짱짱'},
    {'code':'EC0001', 'name':'익명의 고라니', 'address':'g14gdsf', 'level':'2', 'comment':'여친사귈 수 있어요!'},
    {'code':'EC0001', 'name':'익명의 고라니', 'address':'a2v2dh5', 'level':'2', 'comment':'헤헤 재밌었다.'},
    {'code':'EC0001', 'name':'익명의 고라니', 'address':'37kp53b', 'level':'1', 'comment':'최고입니다.'},
    {'code':'EC0001', 'name':'익명의 고라니', 'address':'1nglrp8', 'level':'1', 'comment':'최고의 봉사활동이라 할 수 있습니다.'},
    {'code':'EC0001', 'name':'익명의 고라니', 'address':'blinij9', 'level':'1', 'comment':'별로네요… 힘들어요.'},
    {'code':'BC0001','name':'익명의 고라니','address':'nk6i630','level':'2','comment':'공원 관리가 엉망이네요. 자원 봉사자 관리도 엉망입니다. 개선이 필요합니다.'},
    {'code':'BC0001','name':'익명의 고라니','address':'o0ln4wo','level':'1','comment':'별로 좋지 않네요. 지원금은 다 어디에 쓰는지 도구는 다 부족하고 부러져 있고 상태가 좋지 않습니다.'},
    {'code':'BC0001','name':'익명의 고라니','address':'ks993gg','level':'5','comment':'추천하지 않아요. 힘만들고 적폐 봉사활동입니다.'},
    {'code':'BC0001', 'name':'익명의 고라니', 'address':'ks993gg', 'level':'3', 'comment':'재밌습니다.'},
    {'code':'BC0001', 'name':'익명의 고라니', 'address':'g14gdsf', 'level':'2', 'comment':'도구 좀 보충해주세요… 지원금도 많이 받는거 같던데...'},
    {'code':'HW0001','name':'익명의 고라니','address':'nk6i630','level':'2','comment':'자원봉사자 분들 및 담당자 분들이 다 착하고 좋은 분들이 모이셨습니다. 봉사활동과 인맥쌓기 일석이조의 봉사활동입니다.'},
    {'code':'HW0001','name':'익명의 고라니','address':'o0ln4wo','level':'1','comment':'와 봉사활동하는데 간식도 챙겨주는 곳이 있다니 너무 좋네요. 강추!'},
    {'code':'HW0001','name':'익명의 고라니','address':'ks993gg','level':'5','comment':'연인과 데이트 하며 가볍게 봉사도 할 수 있는 좋은 봉사활동인 것 같습니다.'},
    {'code':'HW0001', 'name':'익명의 고라니', 'address':'ks993gg', 'level':'3', 'comment':'어렵지 않지만 꼭 필요한 활동입니다. 여기서 많이 봉사하세요~'},
    {'code':'SN0001','name':'익명의 고라니','address':'nk6i630','level':'2','comment':'이재명 경기지사님을 가까히 볼 수 있어 신기했어요.'},
    {'code':'SN0001','name':'익명의 고라니','address':'o0ln4wo','level':'1','comment':'법률 상담이라는 좋은 취지의 활동을 도울 수 있어 영광입니다.'},
    {'code':'SN0001','name':'익명의 고라니','address':'ks993gg','level':'5','comment':'오오 도지사님 리스펙'},
    {'code':'SN0001', 'name':'익명의 고라니', 'address':'ks993gg', 'level':'3', 'comment':'재밌습니다.'},
    {'code':'SN0001', 'name':'익명의 고라니', 'address':'g14gdsf', 'level':'2', 'comment':'정말 좋은 활동입니다!'},
    {'code':'SN0001', 'name':'익명의 고라니', 'address':'a2v2dh5', 'level':'2', 'comment':'보람되는 활동이에요.'},
    {'code':'SN0001', 'name':'익명의 고라니', 'address':'37kp53b', 'level':'1', 'comment':'굿'}
]


def signup(args):  # add User
    pg = args['t']
    name = args['name']
    id = args.get('id')
    email = args['email']
    mobile = args['mobile']
    code = args.get('code')
    state = args.get('state')
    ty = args.get('type')
    level = args.get('level')
    if not level:
        level = 0

    check = search(email, mobile)  # check email, mobile already available
    if check == 1:
        print(args)
        print({'StatusCode': '1000', 'Message': 'Sign Up fail, Email already available'})
    elif check == 2:
        print(args)
        print({'StatusCode': '1000', 'Message': 'Sign Up fail, Phone Number already available'})

    if pg == 'p':
        acc = Person(name, id, email, mobile, level)
        acc.private, acc.seed, acc.address, acc.eth = args['private'], args['seed'], args['address'], args['eth']
        Accounts[acc.private] = acc
        # session['code'] = {'seed': acc.seed, 'wallet': acc.address, 'private': acc.private}
        # print(args)
        # print({'StatusCode': '200', 'Message': 'Sign Up success'})

    elif pg == 'g':
        acc = Group(name, code, email, mobile, state, ty)
        acc.msg, acc.comment, acc.history = args['msg'], args.get('com'), int(args['cnt'])
        acc.private, acc.seed, acc.address, acc.eth = args['private'], args['seed'], args['address'], args['eth']
        if args.get('thumb'):
            acc.thumb = args['thumb']
        Accounts[acc.private] = acc
        Groups[acc.code] = acc
        # session['code'] = {'seed': acc.seed, 'wallet': acc.address, 'private': acc.private}
        # print(args)
        # print({'StatusCode': '200', 'Message': 'Sign Up success'})


def event(args):
    # {'code':'SWJA7777', 'eid':'N1e6EzSe', 'startdate':'20180727', 'enddate':'20180829', 'rnum':'5', 'rdnum':'5', 'state':'수원시 장안구', 'activity':'2018년 경기도 블록체인 해커톤 참가 및 우승', 'note':'1등은 우리의 것', 'isactive':'false'},
    code = args['code']
    eid = args['eid']
    startdate = args['startdate']
    enddate = args['enddate']
    rnum = args['rnum']
    rdnum = args['rdnum']
    state = args['state']
    activity = args['activity']
    note = args['note']
    isactive = args['isactive']

    startdate, enddate = ''.join(startdate.split('-')), ''.join(enddate.split('-'))
    if isactive == 'true':
        isactive = True
    else:
        isactive = False

    data = {'code':code, 'eid':eid, 'startdate':startdate, 'enddate':enddate, 'rnum':int(rnum), 'rdnum':int(rdnum), 'state':state, 'activity':activity, 'note':note, 'isactive':isactive}
    # print(data)
    group = Groups.get(code)
    if group:
        Events[eid] = data
        group.events = eid
        Accounts[group.private] = group
        Groups[group.code] = group
        # print('Finished')
    else: print('No Group Founded')


'''
def comment(args):
    # {'code': 'EW0001', 'name': '심건영', 'comment': '해커톤 1등하고 싶어요'}
    code = args['code']
    name = args['name']
    comment = args['comment']

    data = {'code':code, 'name':name, 'comment':comment}
    group = Groups.get(code)
    if group:
        group.comments = data
        Accounts[group.private] = group
        Groups[group.code] = group
        # print('Finished')
    # else: print('No Group Founded')
'''


def comment(args):
    # {'code': 'EW0001', 'name': '심건영', 'address': 'blinij9', 'level': '1', 'comment': '해커톤 1등하고 싶어요'}
    code = args['code']
    name = args['name']
    comment = args['comment']
    address = args['address']
    level = args['level']

    data = {'code':code, 'name':name, 'address': address, 'level': level, 'comment':comment}
    group = Groups.get(code)
    if group:
        group.comments = data
        Accounts[group.private] = group
        Groups[group.code] = group
        # print('Finished')
    else: print('No Group Founded')



def init():
    for u in users:
        signup(u)

    print('[Finished] Add Users from data')

    for e in events:
        event(e)

    print('[Finished] Add Events from data')
    # print(Events)

    for c in comments:
        # print(c, ' : ',end='')
        comment(c)

    print('[Finished] Add Comments from data')
