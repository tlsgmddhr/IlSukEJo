from flask import Flask
from flask_restful import Api
import os

app = Flask(__name__)
api = Api(app)

app.secret_key = os.urandom(16)

from app import page, sign, data, group
