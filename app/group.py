from flask import redirect, session, url_for
from flask_restful import Resource, reqparse
from app import api, page
from app.data import Accounts, Groups


def search(t, s, n):
    results = []
    for key, info in Groups.items():
        result = info.match(t, s, n)
        if result:
            results.append(key)
    return results


class GroupSearch(Resource):
    def post(self):
        try:
            parser = reqparse.RequestParser()
            parser.add_argument('type', type=str)
            parser.add_argument('state', type=str)
            parser.add_argument('name', type=str)
            args = parser.parse_args()

            self.type = str(args['type'])
            self.state = str(args['state'])
            self.name = str(args['name'])
            result = self.search()

        except Exception as e:
            result = {'StatusCode': '2000', 'Message': str(e)}
        print(result)
        return redirect(url_for('home'))

    def search(self):
        result = search(self.type, self.state, self.name)
        if len(result) > 0:
            data = []
            for r in result:
                data.append(Groups[r].title())
            session['search'] = data
            return {'StatusCode': '200', 'Message': 'Search success'}
        return {'StatusCode': '1000', 'Message': 'Search fail, Wrong Information'}

    def get(self):
        try:
            parser = reqparse.RequestParser()
            parser.add_argument('id', type=str)
            args = parser.parse_args()

            self.id = str(args['id'])
            result = self.get_data()

        except Exception as e:
            result = {'StatusCode': '2000', 'Message': str(e)}
        # print(result)
        return result

    def get_data(self):
        g = Groups.get(self.id)
        if g:
            return {'StatusCode': '200', 'Message': 'Search success', 'data':g.data()}
        return {'StatusCode': '1000', 'Message': 'Search fail, Wrong Information'}


api.add_resource(GroupSearch, '/group')


class ChangeThumb(Resource):
    def post(self):
        try:
            parser = reqparse.RequestParser()
            parser.add_argument('id', type=str)
            parser.add_argument('url', type=str)
            args = parser.parse_args()

            self.id = str(args['id'])
            self.url = str(args['url'])
            result = self.update()

        except Exception as e:
            result = {'StatusCode': '2000', 'Message': str(e)}
        print(result)
        return redirect(url_for('my_page'))

    def update(self):
        group = Groups.get(self.id)
        if group:
            group.thumb = self.url
            Accounts[group.private] = group
            Groups[group.code] = group
            return {'StatusCode': '200', 'Message': 'Update Thumbnail success'}
        return {'StatusCode': '1000', 'Message': 'Update fail, Wrong Group ID'}


api.add_resource(ChangeThumb, '/thumb')

'''
class Comment(Resource):
    def post(self):
        try:
            parser = reqparse.RequestParser()
            parser.add_argument('code', type=str)
            parser.add_argument('name', type=str)
            parser.add_argument('comment', type=str)
            args = parser.parse_args()

            self.code = str(args['code'])
            self.name = str(args['name'])
            self.comment = str(args['comment'])
            result = self.add()

        except Exception as e:
            result = {'StatusCode': '2000', 'Message': str(e)}
        print(result)
        return redirect(url_for('home'))

    def add(self):
        data = {'code': self.code, 'name': self.name, 'comment': self.comment}
        group = Groups.get(self.code)
        if group:
            group.comments = data
            Accounts[group.private] = group
            Groups[group.code] = group
            return {'StatusCode': '200', 'Message': 'Add Comment success'}
        return {'StatusCode': '1000', 'Message': 'Add Comment fail, Wrong Group ID'}
'''


class Comment(Resource):
    def post(self):
        try:
            parser = reqparse.RequestParser()
            parser.add_argument('code', type=str)
            parser.add_argument('name', type=str)
            parser.add_argument('address', type=str)
            parser.add_argument('level', type=str)
            parser.add_argument('comment', type=str)
            args = parser.parse_args()

            self.code = str(args['code'])
            self.name = str(args['name'])
            self.address = str(args['address'])
            self.level = str(args['level'])
            self.comment = str(args['comment'])
            result = self.add()

        except Exception as e:
            result = {'StatusCode': '2000', 'Message': str(e)}
        print(result)
        return redirect(url_for('home'))

    def add(self):
        data = {'code': self.code, 'name': self.name, 'address': self.address, 'level': self.level, 'comment': self.comment}
        group = Groups.get(self.code)
        if group:
            group.comments = data
            Accounts[group.private] = group
            Groups[group.code] = group
            return {'StatusCode': '200', 'Message': 'Add Comment success'}
        return {'StatusCode': '1000', 'Message': 'Add Comment fail, Wrong Group ID'}


api.add_resource(Comment, '/comment')
