from flask import render_template, redirect, url_for, session
from app import app
from app.data import Accounts, Groups, Events


# session['user'] = {'key':'private_key', 'name':'user_name', 'type':'p' or 'g' (person, group)}
# session['code'] = {'seed':'seed_value', 'wallet':'wallet_address', 'private':'private_key'}
# session['search'] = [{'thumb': self.thumb, 'type': self.type, 'state': self.state, 'name': self.name,}, ... ]


def sess():
    if 'user' in session:
        return session['user']
    return False


def search():
    if 'search' in session:
        return session['search']
    return None


@app.route('/')
def root():  # redirect to /home
    return redirect(url_for('home'))


@app.route('/home')
def home():  # view home page or if no session, redirect to /login
    s = sess()
    if s:
        if s['type'] == 'g':
            return render_template('home.html', session=s, search=search(), group=Accounts[s['key']].data())
        return render_template('home.html', session=s, search=search())
    return redirect(url_for('login'))


@app.route('/login')
def login():
    if not sess():  # if session, redirect to /home or view login page
        return render_template('login.html', session=None)
    return redirect(url_for('home'))


@app.route('/logout')
def logout():  # logout and redirect to home (may redirect to login page)
    session.pop('user', None)
    return redirect(url_for('home'))


@app.route('/signup')
def signup_type():  # signup select type page
    if not sess():  # if session, redirect to /home or view signup type page
        return render_template('signup_type.html')
    return redirect(url_for('home'))


@app.route('/signup/<value>')
def signup_rout(value):  # signup pages
    if not sess():  # if session, redirect to /home or view signup form page
        if value == 'person':  # signup person form
            return render_template('signup_person.html')
        elif value == 'group':  # signup group form
            return render_template('signup_group.html')
        elif value == 'confirm':  # signup mobile phone confirm page
            return render_template('signup_mobile.html')
        elif value == 'welcome':  # after finish signup, welcome page
            return redirect(url_for('home'))
    return redirect(url_for('home'))


@app.route('/term/<num>')
def terms(num=None):  # view term page
    if num:
        return render_template('term_{}.html'.format(num))
    return redirect(url_for('home'))


@app.route('/view_code')
def view_code():
    if 'code' in session:  # check codes in session
        codes = session['code']  # copy to value
        session.pop('code', None)  # remove 'code' session
        return render_template('view_code.html', codes=codes)
    return redirect(url_for('home'))


@app.route('/lost_pw')
def lost_pw():
    if not sess():  # check logged in
        return render_template('lost_pw.html')
    return redirect(url_for('home'))


@app.route('/charge')
def charge():
    if sess():  # check logged in
        return render_template('charge.html', session=sess())
    return redirect(url_for('home'))


@app.route('/refund')
def refund():
    if sess():  # check logged in
        return render_template('refund.html', session=sess())
    return redirect(url_for('home'))


@app.route('/my_page')
def my_page():
    s = sess()
    if s:  # check logged in
        info = Accounts.get(s['key'])  # receive account info
        if info:
            return render_template('my_page.html', session=s, info=info.my_page())
        # error page..?
    return redirect(url_for('home'))


@app.route('/donate/<group_code>')
def donate(group_code):
    if sess():  # check logged in
        info = Groups.get(group_code)  # receive group info
        if info:
            return render_template('donate.html', session=sess(), info=info.d_info())
        # error page..?
    return redirect(url_for('home'))


@app.route('/event')
def new_event():
    if sess():  # check logged in
        # generate new event_id
        event_id = ''
        redirect('/event/' + event_id)
    return redirect(url_for('home'))


@app.route('/event/<event_id>')
def event(event_id):
    s = sess()
    if s:  # check logged in
        e = Events.get(event_id)
        if e:
            if s['type'] == 'p':  # person
                if e['rnum'] < e['rdnum']:
                    e['rdnum'] = e['rdnum'] + 1
                return render_template('event_result.html', event=e)
            elif s['type'] == 'g':  # group
                return render_template('event_edit.html', session=s, event=e, group_id=Accounts[s['key']].code,
                                event_id=event_id)
    return redirect(url_for('home'))