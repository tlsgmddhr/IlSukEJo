from flask import redirect, session, url_for
from flask_restful import Resource, reqparse
from app import api
from app.data import Accounts, Groups, Events
from app.keyword import get_keyword
import string
import random


def search(email, mobile):
    for key, info in Accounts.items():
        result = info.search(email, mobile)
        if result:
            return result
    return False


def lost(seed, email):
    for key, info in Accounts.items():
        result = info.lost(seed, email)
        if result:
            return {'seed':info.seed, 'wallet':info.address, 'private':info.private}
    return False


class Account:
    def __init__(self, t, n, e, m):
        self.t = t
        self.name = n
        self.email = e
        self.mobile = m
        self.private = self.generator(8)
        self.seed, self.address = self.generate_add()
        self.eth = 0

    @staticmethod
    def generator(n):
        s = string.ascii_letters + string.digits
        k = ''.join([random.choice(s) for _ in range(n)])
        return k

    @staticmethod
    def generate_add():
        # get seed and address from block chain
        return Account.generator(20), Account.generator(20)

    def lost(self, s, e):
        if self.seed == s and self.email == e:
            self.private = self.generator(8)
            return True
        return False

    def search(self, e, m):
        if self.email == e:
            return 1
        elif self.mobile == m:
            return 2
        return False


class Person(Account):
    def __init__(self, n, i, e, m, l=0):
        super().__init__(t='p', n=n, e=e, m=m)
        self.id = i
        self.rank = int(l)

    def info(self):
        i = {
            'key': self.private,
            'name': self.name,
            'type': self.t,
            'address': self.address,
            'level': self.rank
        }
        return i

    def my_page(self):
        m = {
            'name': self.name,
            'rank': self.rank,
            'wallet': self.address,
            'eth': self.eth
        }
        return m


class Group(Account):
    def __init__(self, n, c, e, m, s, t):
        super().__init__(t='g', n=n, e=e, m=m)
        self.code = c
        self.state = s
        self.type = t
        self.thumb = 'http://www.google.co.kr/images/branding/googlelogo/2x/googlelogo_color_120x44dp.png'
        self.msg = ''
        self.comment = ''
        self._events = []
        self._comments = []
        self.history = 0
        self.keyword = ''
        self._meanDict = {}

    @property
    def events(self):
        data = []
        for e in self._events:
            data.append(Events.get(e))
        return data

    @events.setter
    def events(self, eid):
        self._events.append(eid)

    @property
    def comments(self):
        return self._comments

    @comments.setter
    def comments(self, data):
        self._comments.append(data)
        self.keyword, self._meanDict = get_keyword(self.comments, self._meanDict)
        # print('key',self.keyword)
        # print('mean',self._meanDict)

    def info(self):
        i = {
            'key': self.private,
            'name': self.name,
            'type': self.t,
            'address': self.address,
            'level': '99'
        }
        return i

    def match(self, type, state, name):
        # print(self.type, self.state, self.name,'\n', type, state, name, '\n', type in self.type, state in self.state, name in self.name)
        return (type in self.type or not len(type)) and (state in self.state or not len(state)) and (name in self.name or not len(name))

    def title(self):
        t = {
            'thumb': self.thumb,
            'type': self.type,
            'state': self.state,
            'name': self.name,
            'code': self.code
        }
        return t

    def data(self):
        d = {
            'thumb': self.thumb,
            'msg': self.msg,
            'comment': self.comment,
            'events': self.events,
            'comments': self.comments,
            'keywords': self.keyword
        }
        return d

    def d_info(self):
        d = {
            'name': self.name,
            'state': self.state,
            'type': self.type,
            'message': self.msg,
            'comment': self.comment,
            'history': self.history
        }
        return d

    def my_page(self):
        m = {
            'name': self.name,
            'wallet': self.address,
            'eth': self.eth,
            'state': self.state,
            'type': self.type,
            'thumb': self.thumb,
            'msg': self.msg,
            'code': self.code
        }
        return m


class SignIn(Resource):
    def post(self):
        try:
            parser = reqparse.RequestParser()
            parser.add_argument('type', type=str)
            parser.add_argument('key', type=str)
            args = parser.parse_args()

            self.type = str(args['type'])
            self.key = str(args['key'])

            if self.type != 'LOGIN':
                raise Exception('Wrong Access')
            result = self.signin()

        except Exception as e:
            result = {'StatusCode': '2000', 'Message': str(e)}
        print(result)
        return redirect(url_for('home'))

    def signin(self):
        acc = Accounts.get(self.key)
        if acc:
            session['user'] = acc.info()
            return {'StatusCode': '200', 'Message': 'Login success'}
        return {'StatusCode': '1000', 'Message': 'Login fail, Wrong Private Key'}


api.add_resource(SignIn, '/signin')


class SignUp(Resource):
    def post(self):
        try:
            parser = reqparse.RequestParser()
            tmp = parser
            tmp.add_argument('pg', type=str)
            args = tmp.parse_args()
            self.pg = args['pg']

            if self.pg == 'p':
                parser.add_argument('name', type=str)
                parser.add_argument('id', type=str)
                parser.add_argument('email', type=str)
                parser.add_argument('mobile', type=str)
                args = parser.parse_args()
                self.name = args['name']
                self.id = args['id']
                self.email = args['email']
                self.mobile = args['mobile']
            elif self.pg == 'g':
                parser.add_argument('name', type=str)
                parser.add_argument('code', type=str)
                parser.add_argument('email', type=str)
                parser.add_argument('mobile', type=str)
                parser.add_argument('state', type=str)
                parser.add_argument('type', type=str)
                args = parser.parse_args()
                self.name = args['name']
                self.code = args['code']
                self.email = args['email']
                self.mobile = args['mobile']
                self.state = args['state']
                self.type = args['type']
            else:
                raise Exception('Wrong Access')
            result = self.signup()

        except Exception as e:
            result = {'StatusCode': '2000', 'Message': str(e)}
        print(result)
        if result['StatusCode'] == '200':
            return redirect(url_for('view_code'))
        elif self.pg == 'p':
            return redirect('/signup/person')
        elif self.pg == 'g':
            return redirect('/signup/group')
        return redirect(url_for('view_code'))

    def signup(self):
        check = search(self.email, self.mobile)  # check email, mobile already available
        if check == 1:
            return {'StatusCode': '1000', 'Message': 'Sign Up fail, Email already available'}
        elif check == 2:
            return {'StatusCode': '1000', 'Message': 'Sign Up fail, Phone Number already available'}

        if self.pg == 'p':
            acc = Person(self.name, self.id, self.email, self.mobile)
            chk = Accounts.get(acc.private)
            while chk:
                acc.lost(acc.seed, acc.email)
                chk = Accounts.get(acc.private)
            Accounts[acc.private] = acc
            session['code'] = {'seed':acc.seed, 'wallet':acc.address, 'private':acc.private}
            return {'StatusCode': '200', 'Message': 'Sign Up success'}

        elif self.pg == 'g':
            acc = Group(self.name, self.code, self.email, self.mobile, self.state, self.type)
            chk = Accounts.get(acc.private)
            while chk:
                acc.lost(acc.seed, acc.email)
                chk = Accounts.get(acc.private)
            Accounts[acc.private] = acc
            Groups[acc.code] = acc
            session['code'] = {'seed':acc.seed, 'wallet':acc.address, 'private':acc.private}
            return {'StatusCode': '200', 'Message': 'Sign Up success'}

        return {'StatusCode': '1000', 'Message': 'Sign Up fail, Wrong Type'}


api.add_resource(SignUp, '/signup')


class LostPW(Resource):
    def post(self):
        try:
            parser = reqparse.RequestParser()
            parser.add_argument('seed', type=str)
            parser.add_argument('email', type=str)
            args = parser.parse_args()

            self.seed = str(args['seed'])
            self.email = str(args['email'])
            result = self.lostpw()

        except Exception as e:
            result = {'StatusCode': '2000', 'Message': str(e)}
        print(result)
        if result['StatusCode'] == '200':
            return redirect(url_for('view_code'))
        return redirect(url_for('lost_pw'))

    def lostpw(self):
        result = lost(self.seed, self.email)
        if result:
            session['code'] = result
            return {'StatusCode': '200', 'Message': 'Password found success'}
        return {'StatusCode': '1000', 'Message': 'Password found fail, Wrong information'}


api.add_resource(LostPW, '/lostpw')
