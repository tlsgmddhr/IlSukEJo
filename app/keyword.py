from konlpy.tag import Okt
from collections import Counter
import os

def minusdict(a,b):
    #a-b
    temp = dict()
    for i in a:
        if i in b:
            temp[i] = a[i] -b[i]
    a.update(temp)
    try:
        Min = min(a.values())
    except:
        Min = 0
    for i in a:
        a[i] =a[i] - Min + 0.001
    return a
def plusdict(a,b):
    #a+b
    temp = dict()
    for i in b:
        if i in a:
            temp[i] = a[i]+b[i]
    a.update(b)
    a.update(temp)
    return a
def resize(a):
    Sum = sum(a.values())
    Len  = len(a)
    if Sum ==0:
        return a
    for i in a:
        a[i] = len(a) * a[i] / Sum
    return a
def resize100(a):
    Sum = sum(a.values())
    if Sum ==0:
        return a
    for i in a:
        a[i] = 100 * a[i] / Sum
    return a

def get_keyword(comments, meanDict):
    # 게시글 받아오기
    '''
    게시글 = [
    {'code':'EW0001','pid':'VET3Z46O','cid':'00000000','comment':'해커톤 1등하고 싶어요'},
    {'code':'ISEJ0001','pid':'48293433','cid':'00000001','comment':'해커톤 1등하면 좋아요'},
    {'code':'ISEJ0001','pid':'FTI0R7M2','cid':'00000002','comment':'우리가 해커톤 1등이라니!'},
    {'code':'ISEJ0001','pid':'LS827AKJ','cid':'00000003','comment':'기부도 하고 봉사도 하고'},
    {'code':'ISEJ0001','pid':'48734592','cid':'00000004','comment':'사용자 친화적 UI'},
    {'code':'ISEJ0001','pid':'89332154','cid':'00000005','comment':'투명한 봉사활동 관리'},
    {'code':'ISEJ0001','pid':'VET3Z46O','cid':'00000006','comment':'투명한 기부내역 확인'},
    {'code':'ISEJ0001','pid':'LS827AKJ','cid':'00000007','comment':'투명한 봉사단체 후기작성 및 확인'},
    {'code':'ISEJ0001','pid':'FTI0R7M2','cid':'00000008','comment':'편리한 기부 방식'},
    {'code':'ISEJ0001','pid':'48293433','cid':'00000009','comment':'즐거운 봉사활동 해요'},
    {'code':'OS0001','pid':'48734592','cid':'00000010','comment':'투명한 봉사활동 관리'},
    {'code':'OS0001','pid':'89332154','cid':'00000011','comment':'여기 관계자분 디지털티 입고 깎아주시는데 정말 잘 깎으시는 것 같아요. 역시 군인'}
]
    '''

    # 평균 반응 받아오기
    meanDict = plusdict(meanDict,{"해커" : 1, "봉사" : 1})

    nlp = Okt()
    #print(dir(nlp))
    tmp = []
    for  i in range(len(comments)):
        data = nlp.phrases(comments[i]['comment'])
        tmp.extend(data)
    count = Counter(tmp)
    wordInfo = dict()
    for tags, counts in count.most_common(50):
        if (len(str(tags)) > 1):
            wordInfo[tags] = counts
    wordInfo = resize(wordInfo)
    result = resize100(minusdict(wordInfo,meanDict))
    meanDict = resize(plusdict(meanDict,wordInfo))

    #save meanDict and print result
    return result, meanDict

if __name__ == "__main__":
    main()