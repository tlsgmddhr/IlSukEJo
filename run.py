import sys
assert sys.version_info >= (3, 0, 0), "python > 3.X is need, terminate program"

from app import app
from app.data import init
init()
app.run(host='0.0.0.0', debug=True)