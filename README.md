# IlSukEJo

2018 BlockChain Hackerton

>> ## python lib requirement
- pip install flask
- pip install flask_restful
- pip install konlpy


>> ## About Pages
- check logic.txt
- check UI.pdf


>> ## how to run
- python3 run.py
- accessable url : http://127.0.0.1:5000 or your ip, domian
- may portforwarding needed

<hr>
>> ## session description
- session['user'] = {'key':'private_key', 'name':'user_name', 'type':'p' or 'g' (person, group), 'address':'Wallet Address', 'level':'level(Group=99)'}
- session['code'] = {'seed':'seed_value', 'wallet':'wallet_address', 'private':'private_key'}
- session['search'] = [{'thumb': self.thumb, 'type': self.type, 'state': self.state, 'name': self.name,}, ... ]


>> ### Accounts = {'private_key':Account class obj}
>> ### Groups = {'code':Group class obj
>> ## Account obj
- .t		# (String) 'p' or 'g' (person, group)
- .name		# (String) Name
- .email	# (String) E-mail
- .mobile	# (String) Phone Number
- .private	# (String) Private Key
- .seed		# (String) Seed
- .address	# (String) Wallet Address
- .eth		# (String) ETH Amount
>> #### function
- (static) .generator(n) # (String) generate random n digit code
- (static) .generate_add() # (tuple, 2x String) generate seed and address from block chain
- .info		# (dict) {'key':'private_key', 'name':'user_name', 'type':'p' or 'g' (person, group)} ( session['user'] form )
- .lost(seed, email) # return .info w/ new private_key or False
- .search(email, mobile) # same email - 1, same mobile - 2, neither False

>> ## Person(Account) obj
- .id		# (String) Identity Number
- .my_page	# (dict) { 'name': self.name, 'rank': self.rank, 'wallet': self.address, 'eth': self.eth }

>> ## Group(Account) obj
- .code		# (String) Confirmation Code
- .state	# (String) Group State
- .type		# (String) Group Type
- .msg		# (String) Group Introduction
- .com		# (String) Group Leader Comment
- .cnt		# (Int) Number of Persons who was active
>> #### function
- .match(type, state, name)	# (Bool) match with obj info
- .title	# (dict) { 'thumb': self.thumb, 'type': self.type, 'state': self.state, 'name': self.name}
- .data 	# (dict) { 'thumb': self.thumb, 'msg': self.msg,  'comment': self.comment,  'events': self.events, 'comments': self.comments }
- .d_info	# (dict) { 'name': self.name, 'state': self.state, 'type': self.type, 'message': self.msg, 'comment': self.comment, 'history': self.history }
- .my_page	# (dict) { 'name': self.name, 'wallet': self.address, 'eth': self.eth, 'state': self.state, 'type': self.type, 'thumb': self.thumb, 'msg': self.msg }


>> # REST POST (type : json)
>> ## SignIn
- input : {'type':'LOGIN', 'key':'input private key'}
- success return : {'StatusCode': '200', 'Message': 'Login success'}
- fail return : {'StatusCode': '1000' or '2000', 'Message': 'error message'}

>> ## SignUp
- input : {'pg':'p' or 'g' (person, group), 
	if 'p' -> 'name':'Name', 'id':'Identity Number', 'email':'E-mail', 'mobile':'Phone Number'
	if 'g' -> 'name':'Group Name', 'code':'Confirmation Code', 'email':'E-mail', 'mobile':'Phone Number', 'state':'Group State', 'type':'Group Type'
}
- success return : {'StatusCode': '200', 'Message': 'Login success'}
- fail return : {'StatusCode': '1000' or '2000', 'Message': 'error message'}


> # Request Func ( REST POST ) ( check help_requests.txt )
> #### 토큰, 거래내역, 봉사활동 등록정보, 봉사활동 시간정보, 댓글의 분석점수
>> ## token 
- post data = {'seed':'Seed', 'address':'Wallet Address'}
- receive data = {'token':'amount of token(now)', 'tran':[{'tran_id':'tranjection id', 'amount':'that tranjection amount'}, ...tranjections in last 10 times]}
- OK : code 200, error code..?

>> ## token_send
- post data = {'seed':'Seed', 'address':'Wallet Address', 'recv':'Receiver address', 'amount':'Amount of tokens'}
- receive data = {'token':'amount of token(now)', 'tran_id':'tranjection id'}
- OK : code 200, error code..?

>> ## Group ( REST GET..? )
- post data = {'code':'Group code'}
- receive data = {
	'active':[{'title':'active volunteer title', 'time':(Int) Working hour, 'num':(Int) require persons, 'now':(Int) applied persons, 'data':'other info'}, ... all active list], 
	'inactive':[{'title':..., 'time':(Int) Working hour, 'num':..., 'now':..., 'data':...}, last 10 list..?]
	}
- OK : code 200, error code..?

>> ## Person
- 
- 
- 



